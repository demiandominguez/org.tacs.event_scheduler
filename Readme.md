Se requiere Base de datos (jdbc:mysql://localhost:3306/event_scheduler) configurable en el archivo application.properties

Los requests requieren un header "token" que se le otorga cuando se logea un usuario (para el login no es necesario un token), este persiste en el tiempo con excepcion de que se realize un logout

Para ejecutar el front:
-Parace en la carpeta /front
-npm install
-npm start

Telegram

 *Bot
 @Tacs4EventSchedulerBot

 *Comandos aceptados:
 
	-/mylists - Get user event lists
	-/listevents [List name] - Given a list name return the events in it
	-/search [keyword] - Given a keyword return the next events that are releated to the keyword
	-/add [List name] [event id] - Given a list name and an event id, adds the event to the list
	-/login [username] [password] - Login using username and password
	
 *Ejecutar el bot
  mvn exec:java


CLOUD

 *Backend
 Hosteado en google cloud platform, dentro de un cluster de kubernetes.
 Accesible a través de la ip 35.194.61.236:80.

 *Bot
 Hosteado en google cloud platform, dentro de un cluster de kubernetes.
 Accesible mediante @Tacs4EventSchedulerBot en telegram.


 *Frontend
 Hosteado en firebase.
 Accesible mediante la URL https://event-scheduler-47396.firebaseapp.com/login
 Debe permitirse al navegador cargar secuencias de comandos inseguras.
 En Chrome en la parte derecha de la barra de direcciones, clickear el simbolo del escudo con la cruz y permitir cargar secuencias de
 comandos inseguras.
