import React from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { NavigationBar } from '../NavigationBar';
import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { UsersPage } from '../UsersPage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { ComparePage } from '../ComparePage';
import { AlarmsPage } from '../AlarmsPage';
import { AdminEventsPage } from '../AdminEventsPage';
import { NotificationsPage } from '../NotificationsPage';
import { SearchPage } from '../SearchPage'

class App extends React.Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }

    render() {
        const { alert } = this.props;
        return (
            <div className="jumbotron">
                <div className="container">
                    <div className="col-sm-8 col-sm-offset-2">
                        {alert.message &&
                            <div className={`alert ${alert.type}`}>{alert.message}</div>
                        }
                        <Router history={history}>
                            <div>
                                <NavigationBar/>
                                <PrivateRoute exact path="/" component={HomePage} />
                                <PrivateRoute exact path="/compare" component={ComparePage} />
                                <PrivateRoute exact path="/users" component={UsersPage} />
                                <PrivateRoute exact path="/admin_events" component = {AdminEventsPage}/>
                                <PrivateRoute path="/search" component={SearchPage} />
                                <Route path="/login" component={LoginPage} />
                                <Route path="/register" component={RegisterPage} />
                                <Route path="/alarms" component={AlarmsPage} />
                                <Route path="/notifications" component={NotificationsPage} />
                            </div>
                        </Router>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 