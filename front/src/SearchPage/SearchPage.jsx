import React from 'react';
import { connect } from 'react-redux';
import { eventActions, categoriesActions, eventListActions, alarmActions } from '../_actions';
import { DropdownButton, MenuItem, Button, Panel, PanelGroup, Glyphicon } from 'react-bootstrap';
 
class SearchPage extends React.Component {
    constructor(props) {    
        super(props);

        this.state = {
            filter: {
                "categories": [],
                "subcategories": [],
                "page":1,
                category: "",
                subcategory: ""
            },
            selectedEventList: {
                id: '',
                name: 'Agregar a una lista de eventos',
            },
            submittedAdd: false,
        };

        this.search = this.search.bind(this);
        this.addCategoryAndSubCategory = this.addCategoryAndSubCategory.bind(this);
        this.saveAlarm = this.saveAlarm.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleClickAdd = this.handleClickAdd.bind(this);
        this.deleteCategory = this.deleteCategory.bind(this);
        this.deleteSubCategory = this.deleteSubCategory.bind(this);
        this.previusPage = this.previusPage.bind(this)
        this.nextPage = this.nextPage.bind(this)
    }

    previusPage() {
        const { filter } = this.state;
        const { page } = filter
        this.setState({
            filter:{
                ...filter,
                "page": (page-1)
            }
        });
        this.search()
    }

    nextPage() {
        const { filter } = this.state;
        const { page } = filter
        this.setState({
            filter:{
                ...filter,
                "page": (page+1)
            }
        });
        this.search()
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { filter } = this.state;
        var insertedValue = value;
        if(name.indexOf("date_start") > -1 || name.indexOf("date_end") > -1){
            insertedValue = new Date(value).getTime()
        }
        if(Number.isNaN(insertedValue)){
            insertedValue = null
        }
        this.setState({ submittedAdd: false });
        this.setState({
            filter:{
                ...filter,
                [name]: insertedValue
            },
        });
    }

    addCategoryAndSubCategory() {
        const { filter } = this.state;
        if (filter.category != ""){
            this.setState({
                    filter: {
                        ...filter, 
                        categories: [...filter.categories, ...[filter.category]],
                        category: ""
                    }
                }
            )
        }
        if (filter.subcategory != ""){
            this.setState({
                    filter: {
                        ...filter, 
                        subcategories: [...filter.subcategories, ...[filter.subcategory]],
                        subcategory: ""
                    }
                }
            )
        }
    }

    deleteCategory(id) {
        const { filter } = this.state;
        this.setState({
            filter: { 
                ...filter, 
                categories: filter.categories.filter(function(category) { 
                                    return category != id
                })
            }
        });
    }

    deleteSubCategory(id) {
        const { filter } = this.state;
        this.setState({
            filter: { 
                ...filter, 
                subcategories: filter.subcategories.filter(function(category) { 
                                    return category != id
                })
            }
        });
    }

    search() {
        const { filter } = this.state
        this.props.dispatch(eventActions.get(filter));
    }
    
    saveAlarm(){
        const { filter } = this.state;
        this.setState({ submittedAdd: true });
        
        if (filter.name){
            this.props.dispatch(alarmActions.save(filter));
        }
    }

    componentDidMount() {
        this.props.dispatch(eventListActions.getAll());
        this.props.dispatch(categoriesActions.getCategories());
    }

    handleClickAdd(event){
        const eventId = event.target.id;
        const { selectedEventList } = this.state
        
        if (selectedEventList.id){
            this.props.dispatch(eventListActions.addNewEvent(eventId, selectedEventList));
        }
    }

    handleSelect(eventKey){
        const { eventLists } = this.props
        const eventList = eventLists.items.find((eventList) => eventList.id === eventKey );
        this.setState({
            selectedEventList: eventList
        });
    }

    render() {
        const { events, eventLists, categoriesList, alarms } = this.props;
        const { selectedEventList, filter, submittedAdd } = this.state;
        
        return (
            <div>
                <Panel defaultExpanded='true'>
                    <Panel.Heading>
                        {<Panel.Title toggle>
                            <h3>Busqueda <Glyphicon glyph="chevron-down" /></h3>
                        </Panel.Title>}
                    </Panel.Heading>
                    <Panel.Body collapsible>
                        <form name="form">
                            <div className="col-md-6">
                                <label>Palabra Clave</label>
                                <input onChange={this.handleChange} type="text" className="form-control" name="keyword" value={filter.keyword} />
                            </div>
                            <div className="col-md-6">
                                <label>Precio</label>
                                <select onChange={this.handleChange} type="number" className="form-control" name="price">
                                    <option value=""></option>
                                    <option value="free">Gratis</option>
                                    <option value="paid">Pago</option>
                                </select>
                            </div>
                            <div className="col-md-12" style={{ 'padding-left': '0px', 'padding-right': '0px' }}>
                                <div className="col-md-6">
                                    <div className="col-md-10" style={{ 'padding-left': '0px', 'padding-right': '0px' }}>
                                        <label>Categorías</label>
                                        <select onChange={this.handleChange} type="text" className="form-control" name="category">
                                            <option value=""></option>
                                            {categoriesList.loadingCategories &&
                                                <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                            }
                                            {categoriesList.items &&
                                                categoriesList.items.categories.map((category) =>
                                                    <option value={category.id}>{category.name}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                    <div className="col-md-2" style={{ 'padding-top': '10px' }}>
                                        <br />
                                        <label>
                                            <a onClick={this.addCategoryAndSubCategory}><Glyphicon glyph="plus" /></a>
                                        </label>
                                    </div>
                                    {filter.categories && filter.categories.map(categoryid =>
                                        <div>
                                            <label>
                                                {categoriesList.items.categories.find(function (element) {
                                                    return element.id == categoryid
                                                }).name
                                                }
                                                <a style={{ color: '#FF0000' }} className="center" bsStyle="danger" onClick={() => this.deleteCategory(categoryid)}><Glyphicon glyph="remove" /></a>
                                            </label>
                                            <br />
                                        </div>
                                    )}
                                </div>
                                <div className="col-md-6">
                                    <div className="col-md-10" style={{ 'padding-left': '0px', 'padding-right': '0px' }}>
                                        <label>Subcategorías</label>
                                        <select onChange={this.handleChange} type="text" className="form-control" name="subcategory">
                                            <option value=""></option>
                                            {categoriesList.loadingCategories &&
                                                <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                            }
                                            {categoriesList.items &&
                                                categoriesList.items.subcategories.map((category) =>
                                                    <option value={category.id}>{category.name}</option>
                                                )
                                            }
                                        </select>
                                    </div>
                                    <div className="col-md-2" style={{ 'padding-top': '10px' }}>
                                        <br />
                                        <a onClick={this.addCategoryAndSubCategory}><Glyphicon glyph="plus" /></a>
                                    </div>
                                    {filter.subcategories && filter.subcategories.map(categoryid =>
                                        <div>
                                            <label>
                                                {categoriesList.items.subcategories.find(function (element) {
                                                    return element.id == categoryid
                                                }).name
                                                }
                                                <a style={{ color: '#FF0000' }} bsStyle="danger" onClick={() => this.deleteSubCategory(categoryid)}><Glyphicon glyph="remove" /></a>
                                            </label>
                                            <br />
                                        </div>
                                    )}
                                </div>
                            </div>  
                            <div className="col-md-4">
                                <label>Fecha Inicio</label>
                                <input onChange={this.handleChange} type="date" className="form-control" name="date_start" />
                            </div>
                            <div className="col-md-4">
                                <label>Fecha Fin</label>
                                <input onChange={this.handleChange} type="date" className="form-control" name="date_end" />
                            </div>
                            <div className="col-md-4">
                                <label>Rango de tiempo</label>
                                <select
                                    onChange={this.handleChange} type="text" className="form-control" name="date_keyword"
                                >
                                    <option value=""></option>
                                    <option value="this_week">Esta semana</option>
                                    <option value="next_week">Próxima semana</option>
                                    <option value="this_weekend">Este fin de semana</option>
                                    <option value="next_month">Próximo mes</option>
                                    <option value="this_month">Este mes</option>
                                    <option value="tomorrow">Mañana</option>
                                    <option value="today">Hoy</option>
                                </select>
                            </div>
                            <div className="col-md-4">
                                <label>Orden</label>
                                <select
                                    onChange={this.handleChange} type="text" className="form-control" name="sortBy">
                                    <option value=""></option>
                                    <option value="date">Fecha</option>
                                    <option value="best">Mejores</option>
                                </select>
                            </div>
                            <div className="col-md-4">
                                <br />
                                <Button className="btn btn-success" onClick={this.search}>Buscar</Button>
                            </div>
                            <div className="col-md-12" style={{ 'margin-top': '10px' }}>
                                <label>Nombre Alarma</label>
                                <input onChange={this.handleChange}  style={{ 'margin-top': '5px' }} type="required" className="form-control" name="name" value={filter.name} />
                                <Button className="btn btn-warning" style={{ 'margin-top': '5px' }} onClick={this.saveAlarm}>Guardar alarma</Button>
                                {submittedAdd && !filter.name &&
                                    <em className="text-danger">Se requiere nombre de la alarma</em>
                                }
                                {alarms.loading &&
                                    <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                }   
                                {alarms.name &&
                                    <em className="text-success">Se agregó correctamente la alarma {alarms.name}</em>
                                }   
                            </div>
                        </form>
                    </Panel.Body>
                </Panel>
                {events.loadingSearch &&
                    <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                }
                {events.items &&
                    <form>
                        <div className-md-12>
                            {filter.page > 1 && <a onClick={this.previusPage}> Anterior </a>}
                            <span> Pagina : {filter.page} </span>
                            {events.items.length == 50 && <a onClick={this.nextPage}> Siguiente </a>}
                        </div>
                        <ul>
                            {events.items.map((event) =>
                                <li key={event.id}>
                                    {<p>{event.name}</p>}
                                    {
                                        <div>
                                            <em>Categoria: {event.category}</em>
                                            <br />
                                            <em>Subcategoria: {event.subcategory}</em>
                                            <br />
                                            <em>Fecha de inicio: {event.startDate}</em>
                                            <br />
                                            <em>Fecha de fin: {event.endDate}</em>
                                            <br />
                                            <em>Organiza: {event.organizerId}</em>
                                            <br />
                                            <a href={event.url} target="_blank">Acceder</a>
                                            <br />
                                            {eventLists.items &&
                                                <div>
                                                    <DropdownButton
                                                        id={event.id}
                                                        title={selectedEventList.name}
                                                        onSelect={this.handleSelect}>
                                                        {eventLists.items.map((eventList) =>
                                                            <MenuItem eventKey={eventList.id}>{eventList.name}</MenuItem>
                                                        )}
                                                    </DropdownButton>
                                                    <Button id={event.id} onClick={this.handleClickAdd}>Agregar</Button>
                                                    {event.loadingAdd &&
                                                        <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                                    }
                                                    {event.eventListAdded &&
                                                        <em className="text-success">El evento {event.name} fue correctamente agregado a la lista {event.eventListAdded}</em>
                                                    }
                                                </div>
                                            }
                                        </div>
                                    }
                                </li>
                            )}
                        </ul>
                        <div className-md-12>
                            {filter.page > 1 && <a onClick={this.previusPage}> Anterior </a>}
                            <span> Pagina : {filter.page} </span>
                            {events.items.length == 50 && <a onClick={this.nextPage}> Siguiente </a>}
                        </div>
                    </form>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { events, eventLists, categoriesList, alarms } = state;
    return {
        events,
        eventLists,
        categoriesList,
        alarms
    };
}

const connectedSearchPage = connect(mapStateToProps)(SearchPage);
export { connectedSearchPage as SearchPage };

