import config from 'config';
import { authHeader } from '../_helpers';

export const alarmService = {
    getAll,
    save,
    delete: _delete
};

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${config.apiUrl}/users/alarms`, requestOptions).then(handleResponse);
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/alarms/?alarmId=${id}`, requestOptions).then(handleResponse);
}

function save(alarm) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(alarm),
    };
    return fetch(`${config.apiUrl}/users/alarms`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const responseJson = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = responseJson.message;
            return Promise.reject(error);
        }
        return responseJson.data;
    });
}
