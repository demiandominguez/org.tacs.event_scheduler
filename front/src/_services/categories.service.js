import config from 'config';
import { authHeader } from '../_helpers';
import { handleResponse } from '../_helpers';

export const categoriesService = {
    getCategories
};

function getCategories() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${config.apiUrl}/events/filters`, requestOptions).then(handleResponse);
}
