import config from 'config';
import { authHeader } from '../_helpers';
import { handleResponse } from '../_helpers';

export const eventListService = {
    add,
    update,
    getAll,
    addNewEvent,
    delete: _delete
};

function add(eventListName){

    const eventListDTO = {
        name: eventListName.toUpperCase()
    }

    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(eventListDTO),
    };

    return fetch(`${config.apiUrl}/users/event-lists`, requestOptions).then(handleResponse);
}

function update(eventList){

    const eventListDTO = {
        id: eventList.id,
        name: eventList.name.toUpperCase()
    }

    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(eventListDTO),
    };  

    return fetch(`${config.apiUrl}/users/event-lists`, requestOptions).then(handleResponse); 
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/event-lists`, requestOptions).then(handleResponse);
}

function addNewEvent(eventId, eventListId){
    const addEventDTO = {
        eventId: eventId,
        eventListId: eventListId
    };

    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(addEventDTO),
    };
    return fetch(`${config.apiUrl}/users/event-lists/event`, requestOptions).then(handleResponse);

}

function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/event-lists/?id=${id}`, requestOptions).then(handleResponse);
}
