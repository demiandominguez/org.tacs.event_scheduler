import config from 'config';
import { authHeader } from '../_helpers';
import { handleResponse } from '../_helpers';

export const eventService = {
    get,
    getHistory,
    searchInterested
};

function get(filter) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    var parameters = "";
    if (isNotEmpty(filter.keyword)) {
        parameters += "keyword=" + filter.keyword + "&"
    }
    if (isNotEmpty(filter.location_address)) {
        parameters += "location_address=" + filter.location_address.toLowerCase() + "&"
    }
    if (filter.location_distance != null) {
        parameters += "location_distance=" + filter.location_distance.toLowerCase() + "&"
    }
    if (filter.price != null) {
        parameters += "price=" + filter.price.toLowerCase() + "&"
    }
    if (filter.date_start != null) {
        parameters += "date_start=" + new Date(filter.date_start).getTime() + "&"
    }
    if (filter.date_end != null) {
        parameters += "date_end=" + new Date(filter.date_end).getTime() + "&"
    }
    if (isNotEmpty(filter.date_keyword)) {
        parameters += "date_keyword=" + filter.date_keyword.toLowerCase() + "&"
    }   
    if (isNotEmpty(filter.sortBy)) {
        parameters += "sortBy=" + filter.sortBy.toLowerCase() + "&"
    }
    if (filter.page != null) {
        parameters += "page=" + filter.page + "&"
    }
    if (filter.categories.length != 0) {
        parameters += "categories=" + filter.categories + "&"
    }
    if (filter.subcategories.length != 0) {
        parameters += "subcategories=" + filter.subcategories + "&"
    }

    if (isNotEmpty(parameters)) {
        parameters = "?" + parameters.slice(0,-1);
    }
    return fetch(`${config.apiUrl}/events` + parameters, requestOptions).then(handleResponse);
}

function getHistory(filterBy) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/events/history?filterBy=${filterBy}`, requestOptions).then(handleResponse);
}

function searchInterested(eventId) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/event-lists/interested?eventId=${eventId}`, requestOptions).then(handleResponse);
}

function isNotEmpty(string) {
    return (string != undefined) && (string.trim() != '')
}