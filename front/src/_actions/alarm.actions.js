import { alarmConstants } from '../_constants';
import { alarmService } from '../_services';

export const alarmActions = {
    getAll,
    save,
    delete: _delete
};

function getAll() {
    return dispatch => {
        dispatch(request());
        alarmService.getAll()
            .then(
                alarms => dispatch(success(mapAlarms(alarms))),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: alarmConstants.GETALL_REQUEST } }
    function success(alarms) { return { type: alarmConstants.GETALL_SUCCESS, alarms } }
    function failure(error) { return { type: alarmConstants.GETALL_FAILURE, error } }
}

//TODO: Refactorizar esto quedo muy feo 
function mapAlarms(alarms){
    alarms.forEach(alarm => {
        alarm.criteria.startDate = alarm.criteria.startDate && new Date(alarm.criteria.startDate).toISOString().split('T')[0];
        alarm.criteria.endDate = alarm.criteria.endDate && new Date(alarm.criteria.endDate).toISOString().split('T')[0];
        alarm.criteria.categories = alarm.criteria.categories.join();
        alarm.criteria.categories = alarm.criteria.subcategories.join();
        alarm.notifications = alarm.notifications.map(notification => new Date(notification).toISOString().split('T')[0]);
    });

    return alarms;
}

function save(alarm) {
    return dispatch => {
        dispatch(request(alarm));
        alarmService.save(alarm)
            .then(
                () => { 
                    dispatch(success(alarm.name));
                },
                error => {
                    if (error === 400){
                        dispatch(alertActions.error(errorMsg));
                    }             
                }
            );
    };

    function request() { return { type: alarmConstants.SAVE_REQUEST } }
    function success(alarmName) { return { type: alarmConstants.SAVE_SUCCESS, alarmName } }
}

function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        alarmService.delete(id)
            .then(
                alarm => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: alarmConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: alarmConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: alarmConstants.DELETE_FAILURE, id, error } }
}