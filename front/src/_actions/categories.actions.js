import { categoriesConstants } from '../_constants';
import { categoriesService } from '../_services';

export const categoriesActions = {
    getCategories
};

function getCategories() {
    return dispatch => {
        dispatch(request());
        categoriesService.getCategories()
            .then(
                categoriesList => dispatch(success(categoriesList)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: categoriesConstants.GET_CATEGORIES_REQUEST } }
    function success(categoriesList) { return { type: categoriesConstants.GET_CATEGORIES_SUCCESS, categoriesList } }
    function failure(error) { return { type: categoriesConstants.GET_CATEGORIES_ERROR, error } }
}

