import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    register,
    getAll,
    compare
};

function login(user) {
    return dispatch => {
        dispatch(request());
        userService.login(user)
            .then(
                () => { 
                    dispatch(success());
                    history.push('/');
                },
                error => {
                    if (error === 400){
                        const errorMsg = "Usuario o contraseña inválidos"; //TODO: verificar si es una buena practica esto
                        dispatch(failure(errorMsg));
                        dispatch(alertActions.error(errorMsg));
                    }             
                }
            );
    };

    function request() { return { type: userConstants.LOGIN_REQUEST} }
    function success() { return { type: userConstants.LOGIN_SUCCESS} }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                () => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Se registro con exito')); //TODO: verificar si es una buena practica esto
                },
                error => {
                    if (error === 400){
                        const errorMsg = "El usuario ya existe";
                        dispatch(failure(errorMsg));
                        dispatch(alertActions.error(errorMsg));
                    }             
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}

function compare(idUser1, idUser2) {
    return dispatch => {
        dispatch(request());

        userService.compare(idUser1, idUser2)
            .then(
                commonEvents => dispatch(success(commonEvents)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.COMPARE_REQUEST } }
    function success(commonEvents) { return { type: userConstants.COMPARE_SUCCESS, commonEvents } }
    function failure(error) { return { type: userConstants.COMPARE_FAILURE, error } }
}