import { eventConstants } from '../_constants';
import { eventService } from '../_services';
import { alertActions } from './';
import { mapEvents } from '../_helpers';

export const eventActions = {
    get,
    getHistory,
    searchInterested
};

function get(filter) {
    return dispatch => {
        dispatch(request());
        eventService.get(filter)
            .then(
                events => dispatch(success(mapEvents(events))),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: eventConstants.GET_SEARCH_REQUEST } }
    function success(events) { return { type: eventConstants.GET_SEARCH_SUCCESS, events } }
    function failure(error) { return { type: eventConstants.GET_SEARCH_ERROR, error } }
}

function getHistory(filterBy) {
    return dispatch => {
        dispatch(request());

        eventService.getHistory(filterBy)
            .then(
                amount => dispatch(success(amount)),
                error => {
                    dispatch(failure(error.toString()))
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: eventConstants.GET_HISTORY_REQUEST } }
    function success(amount) { return { type: eventConstants.GET_HISTORY_SUCCESS, amount } }
    function failure(error) { return { type: eventConstants.GET_HISTORY_FAILURE, error } }
}

function searchInterested(eventId) {
    return dispatch => {
        dispatch(request());

        eventService.searchInterested(eventId)
            .then(
                amountOfInterested => dispatch(success(amountOfInterested)),
                error => {
                    dispatch(failure(error.toString()))
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: eventConstants.GET_SEARCH_INTERESTED_REQUEST } }
    function success(amountOfInterested) { return { type: eventConstants.GET_SEARCH_INTERESTED_SUCCESS, amountOfInterested } }
    function failure(error) { return { type: eventConstants.GET_SEARCH_INTERESTED_ERROR, error } }
}
