import { notificationConstants } from '../_constants';
import { notificationService } from '../_services';

export const notificationActions = {
    getAll
};

function getAll() {
    return dispatch => {
        dispatch(request());
        notificationService.getAll()
            .then(
                notifs => dispatch(success(notifs)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: notificationConstants.GETALL_REQUEST } }
    function success(notifications) { return { type: notificationConstants.GETALL_SUCCESS, notifications } }
    function failure(error) { return { type: notificationConstants.GETALL_FAILURE, error } }
}