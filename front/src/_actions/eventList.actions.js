
import { eventListConstants } from '../_constants';
import { eventConstants} from '../_constants';
import { eventListService } from '../_services';
import { alertActions } from './';
import { mapEvents } from '../_helpers'

export const eventListActions = {
    add,
    update,
    getAll,
    addNewEvent,
    delete: _delete
};

function add(eventListName) {
    return dispatch => {
        dispatch(request(eventListName));

        eventListService.add(eventListName)
            .then(
                eventListName => { 
                    dispatch(success(eventListName));
                    dispatch(getAll()); //TODO: Cada vez que agrego uno refresheo la lista, no es performante quizas se podria hacer de otra forma
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(eventListName) { return { type: eventListConstants.ADD_REQUEST, eventListName } }
    function success(eventListName) { return { type: eventListConstants.ADD_SUCCESS, eventListName } }
    function failure(error) { return { type: eventListConstants.ADD_FAILURE, error } }
}

function update(eventList, newName) {
    eventList.name = newName
    return dispatch => {
        dispatch(request(eventList));

        eventListService.update(eventList)
            .then(
                eventList => { 
                    dispatch(success(eventList));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(eventListName) { return { type: eventListConstants.UPDATE_REQUEST, eventListName } }
    function success(eventListName) { return { type: eventListConstants.UPDATE_SUCCESS, eventListName } }
    function failure(error) { return { type: eventListConstants.UPDATE_FAILURE, error } }
}

function addNewEvent(eventId, eventList){
    return dispatch => {
        dispatch(request());
        eventListService.addNewEvent(eventId, eventList.id)
            .then(
                () => { 
                    dispatch(success(eventId, eventList.name));
                },
                error => {
                     dispatch(failure(eventId, error.toString()))
                }
            );
    };

    function request() { return { type: eventConstants.ADD_NEW_REQUEST, eventId } }
    function success(eventId, eventListName) { return { type: eventConstants.ADD_NEW_SUCCESS, eventId, eventListName } }
    function failure(error) { return { type: eventConstants.ADD_NEW_FAILURE, eventId, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());
        eventListService.getAll()
            .then(
                eventLists => dispatch(success(mapEventLists(eventLists))),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: eventListConstants.GETALL_REQUEST } }
    function success(eventLists) { return { type: eventListConstants.GETALL_SUCCESS, eventLists } }
    function failure(error) { return { type: eventListConstants.GETALL_FAILURE, error } }
}

function mapEventLists(eventLists){
    eventLists.forEach(eventList => {
        eventList.events = mapEvents(eventList.events);
    });

    return eventLists;
}

function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        eventListService.delete(id)
            .then(
                () => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: eventListConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: eventListConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: eventListConstants.DELETE_FAILURE, id, error } }
}