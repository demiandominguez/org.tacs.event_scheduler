import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { DropdownButton, MenuItem, Button, Table } from 'react-bootstrap';

class ComparePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedUser1: {
                id: '',
                name: 'Seleccione un usuario'
            },
            selectedUser2: {
                id: '',
                name: 'Seleccione otro usuario'
            },
            submitted: false
        };

        this.handleSelect1 = this.handleSelect1.bind(this);
        this.handleSelect2 = this.handleSelect2.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    handleSelect1(eventKey) {
        this.handleSelect(eventKey, "selectedUser1");
    }

    handleSelect2(eventKey) { //TODO: Evitar crear 2 funciones, y simplifcarlo  en 1
        this.handleSelect(eventKey, "selectedUser2");
    }

    handleSelect(eventKey, selectedUser){
        const { users } = this.props;
        const user = users.items.find((user) => user.id === eventKey );

        this.setState({ submitted: false });
        this.setState({
            [selectedUser]: {
                id: user.id,
                name: user.name
            }
        });
    }

    handleClick(event){
        event.preventDefault();
        this.setState({ submitted: true });

        const { selectedUser1, selectedUser2 } = this.state;

        if (selectedUser1.id && selectedUser2.id && selectedUser1.id != selectedUser2.id ){
            this.props.dispatch(userActions.compare(selectedUser1.id, selectedUser2.id));
        }
    }

    render() {
        const { users } = this.props;
        const { submitted, selectedUser1, selectedUser2 } = this.state;
        return (
            <div>              
                <h3>Compare los eventos entre dos usuarios</h3>
                {users.loading && <em>Cargando usuarios...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                <div>
                <DropdownButton
                    title = {selectedUser1.name}
                    onSelect = {this.handleSelect1}>
                    {users.items.map((user) =>
                        <MenuItem eventKey={user.id}>{user.name}</MenuItem>
                    )}
                </DropdownButton>  
                <DropdownButton              
                    title = {selectedUser2.name}
                    onSelect = {this.handleSelect2}>
                    {users.items.map((user) =>
                        <MenuItem eventKey={user.id}>{user.name}</MenuItem>
                    )}
                </DropdownButton>   
                <Button bsStyle="primary" onClick= {this.handleClick}>Comparar</Button>              
                </div>                                   
                }
                {submitted && selectedUser1.id && selectedUser2.id && selectedUser1.id === selectedUser2.id &&
                    <div className="help-block">Los usuarios deben ser distintos</div>
                } 
                {submitted && (!selectedUser1.id || !selectedUser2.id) &&
                    <div className="help-block">Debe seleccionar dos usuarios para comparar</div>
                } 
                {users.loadingCompare &&
                    <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                }
                {users.commonEvents &&
                    <Table striped bordered condensed hover>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Url</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.commonEvents.map((event) =>
                            <tr key={event.id}>
                                <td>{event.name}</td>
                                <td>{event.url}</td>
                            </tr>
                        )}
                    </tbody>
                </Table>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users } = state;
    return {
        users
    };
}

const connectedComparePage = connect(mapStateToProps)(ComparePage);
export { connectedComparePage as ComparePage };