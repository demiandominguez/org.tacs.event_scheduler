import React, { Component } from 'react'
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux';
import { authHeader } from '../_helpers';
import { authentication } from '../_reducers/authentication.reducer';

class NavigationBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            isAdmin: localStorage.getItem('isAdmin') === 'true'
        }
    }

    componentWillReceiveProps(){
        this.setState({isAdmin: localStorage.getItem('isAdmin') === 'true'});   
    }

    notificationRender() {
        this.setState({ show: false });
    }

    render() {
        //TODO: Refactorizar esto, no se debe hacer así.
        const { isLoginOrRegister } = this.props;
        const { isAdmin } = this.state;

        return (
            <div>
                {!isLoginOrRegister &&
                    <Navbar collapseOnSelect>
                        <Navbar.Header>
                            <Navbar.Brand>
                            <a href="/">Event Scheduler</a>
                            </Navbar.Brand>
                            <Navbar.Toggle/>
                        </Navbar.Header>
                        <Navbar.Collapse>
                            <Nav>
                                <NavItem eventKey={1} href="/">
                                    Mis listas de eventos
                                </NavItem>
                                <NavDropdown eventKey={2} title="Mis Alarmas" id="basic-nav-dropdown">
                                    <MenuItem eventKey={2.1} href="alarms">Ver alarmas</MenuItem>
                                    <MenuItem eventKey={2.2} href="search">Agregar alarmas</MenuItem>
                                </NavDropdown>
                                <NavItem eventKey={3} href="search">
                                    Búsqueda
                                </NavItem>
                                {isAdmin &&
                                    <NavDropdown eventKey={4} title="Admin" id="basic-nav-dropdown">
                                        <MenuItem eventKey={4.1} href="users">Ver usuarios</MenuItem>
                                        <MenuItem eventKey={4.2} href="compare">Comparar usuarios</MenuItem>
                                        <MenuItem eventKey={4.3} href="admin_events">Eventos</MenuItem>
                                    </NavDropdown>
                                }
                            </Nav>
                            <Nav pullRight>
                                <NavItem eventKey={5} href="login">
                                    Logout
                                </NavItem>
                            </Nav>
                        </Navbar.Collapse>
                </Navbar>
                }
          </div>
        )
    }
}

function mapStateToProps(state) {
    const { isLoginOrRegister } = state.authentication;
    return {
        isLoginOrRegister
    };
}

const connectedNavigationBar = connect(mapStateToProps)(NavigationBar);
export { connectedNavigationBar as NavigationBar };