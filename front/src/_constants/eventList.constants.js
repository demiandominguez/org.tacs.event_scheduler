export const eventListConstants = {
    ADD_REQUEST: 'EVENTLISTS_REGISTER_REQUEST',
    ADD_SUCCESS: 'EVENTLISTS_REGISTER_SUCCESS',
    ADD_FAILURE: 'EVENTLISTS_REGISTER_FAILURE',

    GETALL_REQUEST: 'EVENTLISTS_GETALL_REQUEST',
    GETALL_SUCCESS: 'EVENTLISTS_GETALL_SUCCESS',
    GETALL_FAILURE: 'EVENTLISTS_GETALL_FAILURE',

    DELETE_REQUEST: 'EVENTLISTS_DELETE_REQUEST',
    DELETE_SUCCESS: 'EVENTLISTS_DELETE_SUCCESS',
    DELETE_FAILURE: 'EVENTLISTS_DELETE_FAILURE',

    UPDATE_FIELD: "EVENTLISTS_UPDATE_FIELD",

    UPDATE_REQUEST: 'EVENTLISTS_UPDATE_REQUEST',
    UPDATE_SUCCESS: 'EVENTLISTS_UPDATE_SUCCESS',
    UPDATE_FAILURE: 'EVENTLISTS_UPDATE_FAILURE',
};
