import React from 'react';
import { connect } from 'react-redux';
import { eventListActions } from '../_actions';
import { eventListConstants} from '../_constants';
import { PanelGroup, Panel, Table, Button, Modal, Glyphicon} from 'react-bootstrap'

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            eventListName: '',
            submittedNewList: false,
            submittedNewListName: null,
            activeKey: '1',
            activeModal: null,
        };
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSelect = this.handleSelect.bind(this);     
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeNewListName = this.handleChangeNewListName.bind(this);
        this.handleSubmitNewList = this.handleSubmitNewList.bind(this);
        this.handleSubmitNewListName = this.handleSubmitNewListName.bind(this);
        this.handleDeleteEventList = this.handleDeleteEventList.bind(this);
    }
    
    componentDidMount() {
        this.props.dispatch(eventListActions.getAll());
    }

    handleClose() {
        this.setState({ activeModal: null });
    }

    handleShow(e, index) {
        this.setState({ activeModal: index })
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({
            submittedNewList: false,
            [name]: value
            });
    }

    handleChangeNewListName(id, event) {
        const { value } = event.target;

        this.props.dispatch(updateField(id, value));
        
        function updateField(id, name) { return { type: eventListConstants.UPDATE_FIELD, id, name } }
    }

    handleSubmitNewList(event) {
        event.preventDefault();
        const { eventListName } = this.state;
        const { dispatch } = this.props;
        const { eventLists } = this.props
        if (eventListName && !eventLists.items.some(list => list.name == eventListName.toUpperCase()) ) {
            this.setState({ submittedNewList: false });
            dispatch(eventListActions.add(eventListName.toUpperCase()));
        } else 
            this.setState({ submittedNewList: true });
    }

    handleSubmitNewListName(eventList, event) {
        event.preventDefault();
        this.setState({ submittedNewListName: eventList.id});

        const { eventLists, dispatch } = this.props;

        if (eventList.newEventListName && !eventLists.items.some(list => list.name == eventList.newEventListName.toUpperCase())) {
            dispatch(eventListActions.update(eventList, eventList.newEventListName.toUpperCase()));
            this.setState({ submittedNewListName: null});
        }
    }

    handleDeleteEventList(id) {
        return () => this.props.dispatch(eventListActions.delete(id));
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    render() {
        const { eventLists } = this.props;
        const { submittedNewList, eventListName, submittedNewListName  } = this.state;

        return (
            <div>              
                <div>
                    <h3>Mis listas de eventos</h3>
                    <form name="form" onSubmit={this.handleSubmitNewList}>
                        <div className={'form-group' + (submittedNewList && (!eventListName || eventLists.items.some(list => list.name == eventListName.toUpperCase()))? ' has-error' : '')}>
                            <input placeholder="Ingrese un nombre para la nueva lista" type="text" 
                                className="form-control" name="eventListName" 
                            value={eventListName} onChange={this.handleChange} />
                            {submittedNewList && !eventListName &&
                                <div className="help-block">Se requiere nombre de la lista de eventos</div>
                            }
                            {submittedNewList && eventLists.items.some(list => list.name == eventListName.toUpperCase()) &&
                                <div className="help-block">Ya existe una Lista con ese nombre</div>
                            }
                            <button className="form-control btn btn-success"><Glyphicon glyph="plus" /></button>
                            {eventLists.loadingAdding &&
                                <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                            }
                        </div>
                    </form>
                </div>     
                <div>               
                    {eventLists.loading &&
                        <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                    }
                    {eventLists.items &&  
                    <ul>
                        {eventLists.items.map((eventList) =>
                        <PanelGroup
                        key={eventList.id}
                        accordion
                        id="accordion-controlled-example"
                        activeKey={this.state.activeKey}
                        onSelect={this.handleSelect}
                        >
                            <Panel eventKey={eventList.id}>
                                <Panel.Heading>
                                {<Panel.Title toggle>
                                    {eventList.name}
                                </Panel.Title>}
                                    <form name="form" onSubmit={(event) => this.handleSubmitNewListName(eventList, event)}>
                                        <div className={'form-group' + (submittedNewListName == eventList.id && (!eventList.newEventListName || eventLists.items.some(list => list.name == eventList.newEventListName.toUpperCase()))? ' has-error' : '')}>
                                            <input key ={eventList.id} placeholder="Ingresar nuevo nombre" type="text" className="form-control"
                                            value={eventList.newEventListName} onChange={(event) => this.handleChangeNewListName(eventList.id, event)} />
                                            {submittedNewListName == eventList.id && !eventList.newEventListName &&
                                                <div className="help-block">Se requiere nombre de la lista de eventos</div>
                                            }
                                            {submittedNewListName == eventList.id && eventList.newEventListName && eventLists.items.some(list => list.name == eventList.newEventListName.toUpperCase()) &&
                                                <div className="help-block">Ya existe una Lista con ese nombre</div>
                                            }
                                            <button className="btn btn-warning">Cambiar nombre</button>
                                            {eventList.deleting ? <em> - Eliminando...</em>
                                            : eventList.deleteError ? <span className="error"> - ERROR: {eventList.deleteError}</span>
                                            : <Button bsStyle="danger" onClick={this.handleDeleteEventList(eventList.id)}><Glyphicon glyph="remove" /></Button>
                                        }</div>
                                    </form>
                                </Panel.Heading>
                                <Panel.Body collapsible>
                                    {eventList.events && 
                                        <Table responsive>
                                            <thead>
                                                <tr>
                                                <th>Nombre Evento</th>
                                                <th>Categoria</th>
                                                <th>Inicio</th>
                                                <th>Detalles</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {eventList.events.map((event) =>
                                                <tr>
                                                <td>{event.name}</td>
                                                <td>{event.category}</td>
                                                <td>{event.startDate}</td>
                                                <td>
                                                    <Button bsStyle="info" bsSize="large" onClick={e => this.handleShow(e, event.id)}>
                                                        <Glyphicon glyph="zoom-in" />
                                                    </Button>
                                                    <Modal id={event.id} show={this.state.activeModal === event.id} onHide={this.handleClose}>
                                                        <Modal.Header closeButton>
                                                            <Modal.Title>{event.name}</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                            <h4>Fecha:          {event.startDate} hasta {event.endDate}</h4>
                                                             <p>Gratis:         {event.free}</p>
                                                             <p>Categoría:      {event.category}</p>
                                                             <p>Subcategoría:   {event.subcategory}</p>
                                                             <p>Link al página del evento: {event.url}</p>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                            <Button onClick={this.handleClose}>Close</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                </td>
                                                </tr>
                                            )}
                                            </tbody>
                                        </Table>
                                    }
                                </Panel.Body>
                            </Panel>
                        </PanelGroup>

                        )}
                    </ul>               
                    }
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { eventLists } = state;
    return {
        eventLists
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage)
export { connectedHomePage as HomePage }