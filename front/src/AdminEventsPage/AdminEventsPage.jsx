import React from 'react';
import { connect } from 'react-redux';
import { eventActions } from '../_actions';
import { DropdownButton, MenuItem} from 'react-bootstrap';

class AdminEventsPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filters: [
                {
                    id: 1,
                    text: 'En el día de hoy',
                    queryParm: 'today',
                },
                {
                    id: 2,
                    text: 'En los últimos 3 días',
                    queryParm: 'lastThreeDays',
                },
                {
                    id: 3,
                    text: 'En la última semana',
                    queryParm: 'lastWeek',
                },
                {
                    id: 4,
                    text: 'En el último mes',
                    queryParm: 'lastMonth',
                },
                {
                    id: 5,
                    text: 'Desde el inicio de los tiempos',
                    queryParm: 'sinceBeginningOfTime',
                },
            ],
            selectedFilter: {
                id: '',
                text: 'Seleccione algún filtro',
                queryParm: '',
            },
            eventId: '',
            selectedEventId: '',
            submitted: false
        };

        this.handleSelect= this.handleSelect.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSelect(eventKey){
        const filter = this.state.filters.find((filter) => filter.id === eventKey );
        this.setState({
            selectedFilter: filter
        });

        this.props.dispatch(eventActions.getHistory(filter.queryParm));       
    }

    handleChange(event){
        const { name, value } = event.target;
        this.setState({
            submitted: false, 
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { eventId } = this.state;

        if (eventId && eventId.length != 0) {
            this.props.dispatch(eventActions.searchInterested(eventId));
            this.setState({selectedEventId: eventId});
        } 
    }

    render() {
        const { events } = this.props;
        const { filters, selectedFilter, submitted, eventId, loadingSearchInterested, selectedEventId } = this.state;
        const hasInputError = submitted && (!eventId || eventId.length == 0);
        return (
            <div>              
                <div>
                <h3>Cantidad total de eventos registrados en el sistema</h3>
                <DropdownButton
                    title = {selectedFilter.text}
                    onSelect = {this.handleSelect}>
                    {filters.map((filter) =>
                        <MenuItem eventKey={filter.id}>{filter.text}</MenuItem>
                    )}
                </DropdownButton> 
                {events.loading &&
                    <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                }
                {events.amount != undefined &&
                <div>
                    <em>Hay {events.amount} evento/s registrados {selectedFilter.text}</em>    
                </div>
                }
                </div>
                <div>
                    <h3>Cantidad de usuarios interesados en un evento</h3>
                        <form name="form" onSubmit={this.handleSubmit}>
                            <div className={'form-group' + (hasInputError ? ' has-error' : '')}>
                                <label htmlFor="eventId">Id del evento</label>
                                <input type="text" className="form-control" name="eventId" value={eventId} onChange={this.handleChange} />
                                {submitted && hasInputError &&
                                    <div className="help-block">Se requeire id del evento</div>
                                }
                                <div className="form-group">
                                    <button className="btn btn-primary">Buscar</button>
                                    {loadingSearchInterested &&
                                        <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                    }
                                </div>
                            </div>
                        </form>
                    {events.loadingSearchInterested &&
                        <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                    }
                    {events.amountOfInterested != undefined &&
                        <div>
                            <em>Hay {events.amountOfInterested} usuarios interesados en el evento con id: {selectedEventId}</em>    
                        </div>
                    }
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { events } = state;
    return {
        events
    };
}

const connectedAdminEventsPage= connect(mapStateToProps)(AdminEventsPage);
export { connectedAdminEventsPage as AdminEventsPage };