import { userConstants } from '../_constants';

export function authentication(state = {}, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        isLoginOrRegister: true,
        loggingIn: true,
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
      };
    case userConstants.LOGIN_FAILURE:
      return {isLoginOrRegister: true};
    case userConstants.LOGOUT:
      return {isLoginOrRegister: true};
    default:
      return state
  }
}