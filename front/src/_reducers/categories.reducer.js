import { categoriesConstants } from '../_constants';

export function categoriesList(state = {}, action) {
  switch (action.type) {
    case categoriesConstants.GET_CATEGORIES_REQUEST: 
      return {
        loadingCategories: true
      };
    case categoriesConstants.GET_CATEGORIES_SUCCESS: 
      return {
        ...state,
        loadingCategories: false,
        items : action.categoriesList
      };
    default:
      return state
  }
}