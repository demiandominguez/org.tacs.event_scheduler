import { userConstants } from '../_constants';

export function users(state = {}, action) {
  switch (action.type) {
    case userConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETALL_SUCCESS:
      return {
        items: action.users
      };
    case userConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };
      case userConstants.COMPARE_REQUEST:
      return {
        ...state,
        loadingCompare: true
      };
    case userConstants.COMPARE_SUCCESS:
      return {
        ...state,
        loadingCompare: false,
        commonEvents: action.commonEvents
      };
    case userConstants.COMPARE_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}