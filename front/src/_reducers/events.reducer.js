import { eventConstants } from '../_constants';

export function events(state = {}, action) {
  switch (action.type) {
    case eventConstants.GET_SEARCH_REQUEST: 
      return {
        loadingSearch: true
      };
    case eventConstants.GET_SEARCH_SUCCESS: 
      return {
        ...state,
        loadingSearch: false,
        items : action.events
      };
    case eventConstants.GET_HISTORY_REQUEST:
      return {
        ...state,
        loading: true
      };
    case eventConstants.GET_HISTORY_SUCCESS:
      return {
        ...state,
        loading: false,
        amount: action.amount
      };
    case eventConstants.GET_HISTORY_FAILURE:
      return { 
        error: action.error
      };
    case eventConstants.GET_SEARCH_INTERESTED_REQUEST:
      return { 
        ...state,
        loadingSearchInterested: true
      };
    case eventConstants.GET_SEARCH_INTERESTED_SUCCESS:
      return { 
        ...state,
        loadingSearchInterested: false,
        amountOfInterested: action.amountOfInterested
      };     
    case eventConstants.GET_SEARCH_INTERESTED_ERROR:
      return { 
        error: action.error
      };
    case eventConstants.ADD_NEW_REQUEST:
      return{
        ...state,
        items: state.items.map(event =>
            event.id === action.eventId
            ? { ...event, loadingAdd: true, eventListAdded: null }
            : event
        )
      };
      case eventConstants.ADD_NEW_SUCCESS:
        return{
          ...state,
          items: state.items.map(event =>
              event.id === action.eventId
              ? { ...event, loadingAdd: false, eventListAdded: action.eventListName }
              : event
          )
      };
      case eventConstants.ADD_NEW_FAILURE:
      return {
        ...state,
        items: state.items.map(event => {
          if (event.id === action.eventId) {         
            const { loadingAdd, ...eventCopy } = event;
            return { ...eventCopy, addNewError: action.error };
          }
          return event;
        })
      };
    default:
      return state
  }
}