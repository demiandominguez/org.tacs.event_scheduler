import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { alarms } from './alarms.reducer';
import { eventLists } from './eventLists.reducer';
import { notifications } from './notificactions.reducer';
import { categoriesList } from './categories.reducer';
import { events } from './events.reducer'

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  eventLists,
  events,
  notifications,
  categoriesList,
  alarms
});

export default rootReducer;