import { notificationConstants } from '../_constants';

export function notifications(state = {}, action) {
  switch (action.type) {
    case notificationConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case notificationConstants.GETALL_SUCCESS:
      return {
        items: action.notifications
      };
    case notificationConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}