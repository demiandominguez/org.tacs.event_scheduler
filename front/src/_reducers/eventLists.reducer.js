import { eventListConstants } from '../_constants';

export function eventLists(state = {}, action) {
  switch (action.type) {
    case eventListConstants.ADD_REQUEST:
      return {
         ...state,
         loadingAdding: true };
    case eventListConstants.ADD_SUCCESS:
      return {
        ...state,
         loadingAdding: false };
    case eventListConstants.ADD_FAILURE:
    return {
      ...state,
       loadingAdding: false };
    case eventListConstants.GETALL_REQUEST:
      return {
        ...state,
        loading: true
      };
    case eventListConstants.GETALL_SUCCESS:
      return {
        ...state,
        loading: false,
        items: action.eventLists
      };
    case eventListConstants.GETALL_FAILURE:
      return { 
        ...state,
        loading:false,
        error: action.error
      };
    case eventListConstants.DELETE_REQUEST:
      return {
        ...state,
        items: state.items.map(eventList =>
            eventList.id === action.id
            ? { ...eventList, deleting: true }
            : eventList
        )
      };
    case eventListConstants.DELETE_SUCCESS:
      return {
        items: state.items.filter(eventList => eventList.id !== action.id)
      };
    case eventListConstants.DELETE_FAILURE:
      return {
        ...state,
        items: state.items.map(eventList => {
          if (eventList.id === action.id) {         
            const { deleting, ...eventListCopy } = eventList;
            return { ...eventListCopy, deleteError: action.error };
          }

          return eventList;
        })
      };
      case eventListConstants.UPDATE_FIELD:
      return {
        ...state,
        items: state.items.map(eventList =>
            eventList.id === action.id
            ? { ...eventList, newEventListName: action.name }
            : eventList
        )
      };
    default:
      return state
  }
}