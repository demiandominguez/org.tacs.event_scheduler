import { alarmConstants } from '../_constants';

export function alarms(state = {}, action) {
  switch (action.type) {
    case alarmConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case alarmConstants.GETALL_SUCCESS:
      return {
        items: action.alarms
      };
    case alarmConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };    
      case alarmConstants.SAVE_REQUEST:
      return {
        name: null,
        loading: true
      };
      case alarmConstants.SAVE_SUCCESS:
        return {
          loading: false,
          name: action.alarmName
        };
      case alarmConstants.SAVE_FAILURE:
        return {
          error: action.error
        };


    case alarmConstants.DELETE_REQUEST:
      // add 'deleting:true' property to alarm being deleted
      return {
        ...state,
        items: state.items.map(alarm =>
          alarm.id === action.id
            ? { ...alarm, deleting: true }
            : alarm
        )
      };
    case alarmConstants.DELETE_SUCCESS:
      // remove deleted alarm from state
      return {
        items: state.items.filter(alarm => alarm.id !== action.id)
      };
    case alarmConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to alarm 
      return {
        ...state,
        items: state.items.map(alarm => {
          if (alarm.id === action.id) {
            // make copy of alarm without 'deleting:true' property
            const { deleting, ...alarmCopy } = alarm;
            // return copy of alarm with 'deleteError:[error]' property
            return { ...alarmCopy, deleteError: action.error };
          }

          return alarm;
        })
      };
    default:
      return state
  }
}