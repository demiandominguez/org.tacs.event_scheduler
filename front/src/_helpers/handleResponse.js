export function handleResponse(response) {
    return response.text().then(text => {
        const responseJson = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                location.reload(true);
            }
            
            const error = response.status;
            return Promise.reject(error);
        }

        return responseJson.data;
    });
}