export function mapEvents(events){
    events.forEach(e => {
        e.startDate = new Date(e.startDate).toLocaleString();
        e.endDate = new Date(e.endDate).toLocaleString();
    });
    
    return events;
}