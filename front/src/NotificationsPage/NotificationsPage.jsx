import React from 'react';
import { connect } from 'react-redux';
import { notificationActions } from '../_actions';

class NotificationsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            notifications : [],
        };
    };

    componentDidMount() {
        this.props.dispatch(notificationActions.getAll());
    }

    render() {
        const { notifications } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                {notifications.loading && <em>Loading notifications...</em>}
                {notifications.items &&
                    <ul>
                        {notifications.items.map((notif) =>
                            <li key={notif.id}>
                                {notif.name}
                            </li>
                        )}
                    </ul>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { notifications } = state;
    return {
        notifications
    };
}

const connectedNotificationsPage = connect(mapStateToProps)(NotificationsPage);
export { connectedNotificationsPage as NotificationsPage };