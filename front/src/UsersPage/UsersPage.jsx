import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import {Table} from 'react-bootstrap';

class UsersPage extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    render() {
        const { users } = this.props;

        return (
            <div>              
                <h3>Total de usuarios registrados</h3>
                {users.loading && <em>Cargando usuarios...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                    <Table striped bordered condensed hover>
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Cantidad de listas</th>
                                <th>Cantidad de alarmas</th>
                                <th>Último acceso</th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.items.map((user) =>
                                <tr key={user.id}>
                                    <td>{user.name}</td>
                                    <td>{user.listsAmount}</td>
                                    <td>{user.alarmsAmount}</td>
                                    <td>{user.lastAccess}</td>
                                </tr>
                            )}
                        </tbody>
                    </Table>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users } = state;
    return {
        users
    };
}

const connectedUsersPage = connect(mapStateToProps)(UsersPage);
export { connectedUsersPage as UsersPage };