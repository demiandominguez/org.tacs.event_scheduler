import React from 'react';
import { connect } from 'react-redux';
import { alarmActions } from '../_actions';
import { PanelGroup, Panel, Table, Button, Modal, Glyphicon} from 'react-bootstrap'

class AlarmsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            alarms : [],
            activeKey: '1',
            activeModal: null
        };

        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    };

    componentDidMount() {
        this.props.dispatch(alarmActions.getAll());
    }

    handleClose() {
        this.setState({ activeModal: null });
    }

    handleShow(index) {
        this.setState({ activeModal: index })
    }

    handleSelect(activeKey) {
        this.setState({ activeKey });
    }

    render() {
        const { alarms } = this.props;
        return (
            <div className="col-lg-12 col-md-12">
                {alarms.loading && <em>Loading alarms...</em>}
                {alarms.items &&
                    <div>
                        {alarms.items.map((alarm) =>
                            <PanelGroup className="responsive"
                                key={alarm.id}
                                accordion
                                activeKey={this.state.activeKey}
                                onSelect={this.handleSelect}>
                                <Panel eventKey={alarm.id}>
                                    <Panel.Heading>
                                        {<Panel.Title toggle>
                                          {alarm.name}
                                        </Panel.Title>}
                                    </Panel.Heading>
                                    <Panel.Body collapsible>
                                        <Table responsive>
                                            <thead>
                                                <tr>
                                                    <th>Keyword</th>
                                                    <th>Fecha inicio</th>
                                                    <th>Fecha fin</th>
                                                    <th>Precio</th>
                                                    <th>Página</th>
                                                    <th>Categorias</th>
                                                    <th>Subcategorias</th>
                                                    <th>Notificaciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{alarm.criteria.keyword}</td>
                                                    <td>{alarm.criteria.startDate}</td>
                                                    <td>{alarm.criteria.endDate}</td>
                                                    <td>{alarm.criteria.price}</td>
                                                    <td>{alarm.criteria.page}</td>
                                                    <td>{alarm.criteria.categories}</td>
                                                    <td>{alarm.criteria.subcategories}</td>
                                                    <td>
                                                        <Button bsStyle="info" bsSize="large" onClick={ () => this.handleShow(alarm.id)}>
                                                            <Glyphicon glyph="bell" />
                                                        </Button>
                                                        <Modal id={alarm.id} show={this.state.activeModal === alarm.id} onHide={this.handleClose}>
                                                        <Modal.Header closeButton>
                                                            <Modal.Title>Notificaciones</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                            <ol>
                                                                {alarm.notifications.map((notification, index) =>   
                                                                    <li key={index}>
                                                                        {'Se encontraron nuevos eventos el día ' + notification}
                                                                    </li>
                                                                )}
                                                            </ol>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                            <Button onClick={this.handleClose}>Close</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </Panel.Body>
                                </Panel>
                            </PanelGroup>
                        )}
                    </div>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { alarms } = state;
    return {
        alarms
    };
}

const connectedAlarmsPage = connect(mapStateToProps)(AlarmsPage);
export { connectedAlarmsPage as AlarmsPage };