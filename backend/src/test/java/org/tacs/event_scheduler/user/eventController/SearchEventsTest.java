package org.tacs.event_scheduler.user.eventController;


import com.google.common.collect.Lists;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestOperations;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.event.EventController;
import org.tacs.event_scheduler.api.dtos.EventDTO;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.repositories.EventbriteRepository;
import org.tacs.event_scheduler.domain.repositories.category.CategoryDao;
import org.tacs.event_scheduler.domain.repositories.category.eventbrite.EventbriteCategoryResponse;
import org.tacs.event_scheduler.domain.repositories.category.eventbrite.EventbriteSubCategoryResponse;
import org.tacs.event_scheduler.domain.repositories.event.EventbriteEventResponse;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SearchEventsTest extends AbstractTest {

    @Autowired
    private EventController eventController;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private EventbriteRepository eventbriteRepository;
    @Mock
    private RestOperations restOperations;
    @Autowired
    private UserService userService;

    private final String TOKEN = this.randomString();

    @Before
    public void setup() {
        this.eventbriteRepository.setRestOperations(this.restOperations);
        this.categoryDao.deleteAll();
        this.userService.deleteAll();
        User user = new User();
        user.setToken(TOKEN);
        this.userService.save(user);
    }

    /*
    @Test
    public void success() {
        String keyword = this.randomString();
        List<String> categories = this.randomStringList();
        List<String> subcategories = this.randomStringList();
        String price = this.randomString();
        String sortBy = this.randomString();
        Integer page = this.randomInteger(10);
        Long startDate = this.randomInteger(10).longValue();
        Long endDate = this.randomInteger(10).longValue();

        EventbriteEventResponse eventbriteEventResponse = this.readDTO("searchEventsTest/EventResponse.json", EventbriteEventResponse.class);
        EventbriteCategoryResponse categoryResponse = this.readDTO("searchEventsTest/CategoriesResponse.json", EventbriteCategoryResponse.class);
        EventbriteSubCategoryResponse subCategoryResponse = this.readDTO("searchEventsTest/SubcategoriesResponse.json", EventbriteSubCategoryResponse.class);
        GenericResponseDTO responseDTO = this.readDTO("searchEventsTest/ResponseDTO.json", GenericResponseDTO.class);

        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteEventResponse.class))).thenReturn(ResponseEntity.ok(eventbriteEventResponse));
        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteCategoryResponse.class))).thenReturn(ResponseEntity.ok(categoryResponse));
        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteSubCategoryResponse.class))).thenReturn(ResponseEntity.ok(subCategoryResponse));

        GenericResponseDTO response = this.eventController.getEvents(TOKEN, keyword, categories, subcategories, price, sortBy, page, startDate, endDate, null);
        assertEquals(toStringCompare(responseDTO), toStringCompare(response));
    }
    
    @Test
    public void successCategoriesInBD() {
        String keyword = this.randomString();
        List<String> categories = this.randomStringList();
        List<String> subcategories = this.randomStringList();
        String price = this.randomString();
        String sortBy = this.randomString();
        Integer page = this.randomInteger(10);
        Long startDate = this.randomInteger(10).longValue();
        Long endDate = this.randomInteger(10).longValue();

        EventbriteEventResponse eventbriteEventResponse = this.readDTO("searchEventsTest/EventResponse.json", EventbriteEventResponse.class);
        EventbriteCategoryResponse categoryResponse = this.readDTO("searchEventsTest/CategoriesResponse.json", EventbriteCategoryResponse.class);
        EventbriteSubCategoryResponse subCategoryResponse = this.readDTO("searchEventsTest/SubcategoriesResponse.json", EventbriteSubCategoryResponse.class);
        GenericResponseDTO responseDTO = this.readDTO("searchEventsTest/ResponseDTO.json", GenericResponseDTO.class);

        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteEventResponse.class))).thenReturn(ResponseEntity.ok(eventbriteEventResponse));
        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteCategoryResponse.class))).thenReturn(ResponseEntity.ok(categoryResponse));
        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteSubCategoryResponse.class))).thenReturn(ResponseEntity.ok(subCategoryResponse));
        this.eventController.getFilters(TOKEN);
        GenericResponseDTO response = this.eventController.getEvents(TOKEN, keyword, categories, subcategories, price, sortBy, page, startDate, endDate, null);
        assertEquals(toStringCompare(responseDTO), toStringCompare(response));
    }

    @Test
    public void successLimitByMaxQuantity() {
        String keyword = this.randomString();
        List<String> categories = this.randomStringList();
        List<String> subcategories = this.randomStringList();
        String price = this.randomString();
        String sortBy = this.randomString();
        Integer page = this.randomInteger(10);
        Long startDate = this.randomInteger(10).longValue();
        Long endDate = this.randomInteger(10).longValue();

        EventbriteEventResponse eventbriteEventResponse = this.readDTO("searchEventsTest/EventResponse.json", EventbriteEventResponse.class);
        EventbriteCategoryResponse categoryResponse = this.readDTO("searchEventsTest/CategoriesResponse.json", EventbriteCategoryResponse.class);
        EventbriteSubCategoryResponse subCategoryResponse = this.readDTO("searchEventsTest/SubcategoriesResponse.json", EventbriteSubCategoryResponse.class);
        GenericResponseDTO responseDTO = this.readDTO("searchEventsTest/ResponseDTO.json", GenericResponseDTO.class);

        Integer maxQuantity = ((List) responseDTO.getData()).size() - 1;

        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteEventResponse.class))).thenReturn(ResponseEntity.ok(eventbriteEventResponse));
        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteCategoryResponse.class))).thenReturn(ResponseEntity.ok(categoryResponse));
        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteSubCategoryResponse.class))).thenReturn(ResponseEntity.ok(subCategoryResponse));
        GenericResponseDTO response = this.eventController.getEvents(TOKEN, keyword, categories, subcategories, price, sortBy, page, startDate, endDate, maxQuantity);
        assertEquals(maxQuantity,  Integer.valueOf(((List) response.getData()).size()));
    }
    */

    private String toStringCompare(GenericResponseDTO responseDTO) {
        return new Gson().toJson(responseDTO).replaceAll("\"", "").trim();
    }

    @Test
    public void empty() {
        EventbriteEventResponse eventbriteEventResponse = new EventbriteEventResponse();
        eventbriteEventResponse.setEvents(Lists.newArrayList());
        when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteEventResponse.class))).thenReturn(ResponseEntity.ok(eventbriteEventResponse));
        List<EventDTO> response =(List<EventDTO>) this.eventController.getEvents(TOKEN, null, null, null, null, null, null, null, null, null).getData();
        verify(this.restOperations).exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventbriteEventResponse.class));
        assertEquals(0, response.size());
    }
}
