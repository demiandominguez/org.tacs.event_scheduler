package org.tacs.event_scheduler.user.userController;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.UserController;
import org.tacs.event_scheduler.api.dtos.user.LoginDTO;
import org.tacs.event_scheduler.api.dtos.user.UserDTO;
import org.tacs.event_scheduler.domain.services.UserService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LogoutTest extends AbstractTest {

    @Autowired
    private UserController userController;
    @Autowired
    private UserService userService;

    private UserDTO userDTO = new UserDTO();

    @Before
    public void cleanRepository() {
        this.userService.deleteAll();
        userDTO.setName(this.randomString());
        userDTO.setPassword(this.randomString());
        this.userController.newUser(userDTO);
    }

    @Test
    public void success() {
        LoginDTO loginDTO = (LoginDTO) this.userController.login(this.userDTO).getData();
        this.userController.logout(loginDTO.getToken());
        assertNull(this.userService.getAll().get(0).getToken());
    }


    @Test
    public void invalidToken() {
        LoginDTO loginDTO = (LoginDTO)this.userController.login(this.userDTO).getData();
        String token = loginDTO.getToken();
        this.userController.logout(token + "x");
        assertEquals(token, this.userService.getAll().get(0).getToken());
    }

    @Test
    public void nullToken() {
        LoginDTO loginDTO = (LoginDTO) this.userController.login(this.userDTO).getData();
        String token = loginDTO.getToken();
        this.userController.logout(null);
        assertEquals(token, this.userService.getAll().get(0).getToken());
    }
}
