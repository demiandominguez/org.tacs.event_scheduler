package org.tacs.event_scheduler.user.userController;

import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.UserController;
import org.tacs.event_scheduler.api.dtos.user.UserInfoDTO;
import org.tacs.event_scheduler.domain.entities.EventList;
import org.tacs.event_scheduler.domain.entities.user.Role;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alarm;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GetUserTest extends AbstractTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserController userController;

    private final String TOKEN = this.randomString();
    private final Date LOGIN_TIME = DateTime.now().toDate();

    private User user;

    @Before
    public void setup() {
        this.userService.deleteAll();
        User admin = new User();
        admin.setRole(Role.ADMIN);
        admin.setToken(TOKEN);
        this.userService.save(admin);
        this.user = new User();
        this.user.setName(this.randomString());
        this.user.setAlarms(Lists.newArrayList(this.randomAlarm(), this.randomAlarm()));
        this.user.setEventsList(Lists.newArrayList(this.randomEventLists(), this.randomEventLists(), this.randomEventLists()));
        this.user.setLastAccess(LOGIN_TIME);
        this.userService.save(this.user);
    }

    @Test
    public void success() {
        Long userId = this.userService.getByName(this.user.getName()).getId();
        UserInfoDTO response = (UserInfoDTO) this.userController.getUser(TOKEN, userId).getData();
        assertEquals(this.user.getName(), response.getName());
        assertEquals(LOGIN_TIME, response.getLastAccess());
        assertEquals(new Integer(2), response.getAlarmsAmount());
        assertEquals(new Integer(3), response.getListsAmount());
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidUser() {
        this.userController.getUser(TOKEN, (long) -1);
    }

    private EventList randomEventLists() {
        EventList eventList = new EventList();
        eventList.setName(this.randomString());
        return eventList;
    }

    private Alarm randomAlarm() {
        Alarm alarm = new Alarm();
        alarm.setName(this.randomString());
        return alarm;
    }
}
