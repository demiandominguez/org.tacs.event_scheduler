package org.tacs.event_scheduler.user.eventListController;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.EventListController;
import org.tacs.event_scheduler.api.dtos.event.EventListDTO;
import org.tacs.event_scheduler.domain.entities.EventList;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.services.UserService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UpdateEventListTest extends AbstractTest {

    @Autowired
    private EventListController eventListController;
    @Autowired
    private UserService userService;

    private final String NAME = this.randomString();
    private String token;

    @Before
    public void setup() {
        this.userService.deleteAll();
        this.newUser();
        this.newEvent();
    }

    private void newUser() {
        User user = new User();
        user.setName(this.randomString());
        user.setPassword(this.randomString());
        this.userService.save(user);
        this.token = this.userService.login(user.getName(), user.getPassword()).getToken();
    }

    private void newEvent() {
        EventListDTO eventListDTO = new EventListDTO();
        eventListDTO.setName(NAME);
        this.eventListController.newEventList(this.token, eventListDTO);
    }

    @Test
    public void success() {
        User user = this.userService.getAll().get(0);
        EventListDTO eventListDTO = new EventListDTO();
        eventListDTO.setName(NAME + "UPDATE");
        eventListDTO.setId(user.getEventsList().get(0).getId());
        this.eventListController.editEventList(this.token, eventListDTO);
        user = this.userService.getAll().get(0);
        assertEquals(1, user.getEventsList().size());
        EventList eventList = user.getEventsList().get(0);
        assertEquals(eventListDTO.getName(), eventList.getName());
        assertEquals(0, eventList.getEventIds().size());
    }

    @Test
    public void duplicatedName() {
        User user = this.userService.getAll().get(0);
        EventListDTO eventListDTO = new EventListDTO();
        eventListDTO.setName(NAME);
        eventListDTO.setId(user.getEventsList().get(0).getId());
        try {
            this.eventListController.editEventList(this.token, eventListDTO);
            fail();
        } catch (IllegalArgumentException e) {
            user = this.userService.getAll().get(0);
            assertEquals(1, user.getEventsList().size());
            EventList eventList = user.getEventsList().get(0);
            assertEquals(eventListDTO.getName(), eventList.getName());
            assertEquals(0, eventList.getEventIds().size());
        }
    }

    @Test
    public void emptyName() {
        User user = this.userService.getAll().get(0);
        EventListDTO eventListDTO = new EventListDTO();
        eventListDTO.setId(user.getEventsList().get(0).getId());
        try {
            this.eventListController.editEventList(this.token, eventListDTO);
            fail();
        } catch (IllegalArgumentException e) {
            user = this.userService.getAll().get(0);
            assertEquals(1, user.getEventsList().size());
            EventList eventList = user.getEventsList().get(0);
            assertEquals(NAME, eventList.getName());
            assertEquals(0, eventList.getEventIds().size());
        }
    }
}
