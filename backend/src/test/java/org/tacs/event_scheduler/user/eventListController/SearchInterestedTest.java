package org.tacs.event_scheduler.user.eventListController;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestOperations;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.EventListController;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;
import org.tacs.event_scheduler.domain.entities.user.Role;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.repositories.EventbriteRepository;
import org.tacs.event_scheduler.domain.repositories.event.EventResponse;
import org.tacs.event_scheduler.domain.services.EventListService;
import org.tacs.event_scheduler.domain.services.UserService;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SearchInterestedTest extends AbstractTest {

	@Autowired
	private EventListController eventListController;
	@Autowired
	private EventListService eventListService;
	@Autowired
    private EventbriteRepository eventbriteRepository;
    @Autowired
    private UserService userService;
    @Mock
    private RestOperations restOperations;

	private final String TOKEN = this.randomString();

    @Before
    public void setup() {
        this.userService.deleteAll();
        this.eventbriteRepository.setRestOperations(restOperations);
    }
   
    @Test
    public void givenEventIdWhenTwoUsersAreInterestedThenReturnTwo() {
    	//Arrange
    	User user1 = newUser();
    	user1.setRole(Role.ADMIN);
    	user1.setToken(TOKEN);
    	this.userService.save(user1);
    	User user2 = newUser();
    	
    	String eventListNameUser1 = randomString();
    	String eventListNameUser2 = randomString();
    	     
        long eventListIdUser1 = newEventList(eventListNameUser1, user1);       
        long eventListIdUser2 = newEventList(eventListNameUser2, user2);

        String eventId = randomString();

        Mockito.when(this.restOperations.exchange(anyString(),eq(HttpMethod.GET), any(), eq(EventResponse.class))).thenReturn(new ResponseEntity<>(HttpStatus.OK));

        eventListService.addEvent(eventId, eventListIdUser1, user1);
        eventListService.addEvent(eventId, eventListIdUser2, user2);
        
        //Act        
        GenericResponseDTO response = eventListController.searchInterested(TOKEN , eventId);
        int amount= (int) response.getData();
        
        //Assert
        assertEquals(2, amount);
    }
    
    @Test
    public void givenEventIdWhenZeroUsersAreInterestedThenReturnZero(){
    	//Arange
        User user1 = newUser();
        user1.setRole(Role.ADMIN);
        user1.setToken(TOKEN);
        this.userService.save(user1);
    	String eventId = randomString();
    	
    	//Act
        GenericResponseDTO response = eventListController.searchInterested(TOKEN, eventId);
        int amount = (int) response.getData();
    	
        //Assert
        assertEquals(0, amount);
    }
    
    private User newUser() {
        User user = new User();
        user.setName(this.randomString());
        user.setPassword(this.randomString());
        this.userService.save(user);
        return user;
    }
      
    private long newEventList(String eventListName, User user){
    	eventListService.newList(eventListName, user); 
    	return userService.get(user.getId()).getEventsList().get(0).getId();
    }
    
}
