package org.tacs.event_scheduler.user.userController;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestOperations;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.UserController;
import org.tacs.event_scheduler.api.dtos.EventDTO;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;
import org.tacs.event_scheduler.domain.entities.user.Role;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.repositories.EventbriteRepository;
import org.tacs.event_scheduler.domain.repositories.event.EventResponse;
import org.tacs.event_scheduler.domain.services.EventListService;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CompareTest extends AbstractTest {

    @Autowired
    private UserController userController;
    @Autowired
    private UserService userService;
    @Autowired
    private EventListService eventListService;
    @Autowired
    private EventbriteRepository eventbriteRepository;
    @Mock
    private RestOperations restOperations;
    @Value("${eventbrite.url.base}")
    private String baseEventUrl;
    @Value("${eventbrite.path.eventid}")
    private String path;
    @Value("${eventbrite.token}")
    private String tokenEventbrite;

    private User user1;
    private User user2;
    private String TOKEN = this.randomString();

    @Before
    public void setup() {
        this.eventbriteRepository.setRestOperations(this.restOperations);
        userService.deleteAll();
        user1 = newUser();
        user1.setRole(Role.ADMIN);
        user1.setToken(TOKEN);
        this.userService.save(user1);
        user2 = newUser();
    }


    @Test
    public void givenTwoValidUsersIdWhenTwoUsersHaveZeroEventsInCommonThenReturnZero() {
        //Arrange
        long eventListIdUser1 = newEventList(randomString(), user1);
        long eventListIdUser2 = newEventList(randomString(), user2);

        Mockito.when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventDTO.class))).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        Mockito.when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class))).thenReturn(new ResponseEntity<>(HttpStatus.OK));

        eventListService.addEvent("1", eventListIdUser1, user1);
        eventListService.addEvent("2", eventListIdUser1, user1);
        eventListService.addEvent("3", eventListIdUser1, user1);
        eventListService.addEvent("4", eventListIdUser1, user1);

        eventListService.addEvent("5", eventListIdUser2, user2);
        eventListService.addEvent("6", eventListIdUser2, user2);
        eventListService.addEvent("7", eventListIdUser2, user2);
        eventListService.addEvent("8", eventListIdUser2, user2);

        //Act
        GenericResponseDTO response = userController.compare(TOKEN, user1.getId(), user2.getId());
        List<EventDTO> responseEventDTOs = (List<EventDTO>) response.getData();

        //Assert
        assertNotNull(responseEventDTOs);
        assertEquals(0, responseEventDTOs.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenTwoValidUsersIdWhenTwoUsersIdAreEqualsThenThrowIllegalArgumentException() {
        userController.compare(TOKEN, user1.getId(), user1.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenTwoUsersIdIsWhenFirstIdIsInvalidThenThrowIllegalArgumentException() {
        userController.compare(TOKEN, (long) -1, user1.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenTwoUsersIdIsWhenSecondIdIsInvalidThenThrowIllegalArgumentException() {
        userController.compare(TOKEN, user1.getId(), (long) -1);
    }

    private User newUser() {
        User user = new User();
        user.setName(this.randomString());
        user.setPassword(this.randomString());
        this.userService.save(user);
        return user;
    }

    private long newEventList(String eventListName, User user) {
        eventListService.newList(eventListName, user);
        return userService.get(user.getId()).getEventsList().get(0).getId();
    }

    private String getUrl(String id) {
        return this.baseEventUrl + path + id + "?token=" + this.tokenEventbrite;
    }

}
