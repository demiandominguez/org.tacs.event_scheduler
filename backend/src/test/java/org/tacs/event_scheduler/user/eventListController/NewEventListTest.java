package org.tacs.event_scheduler.user.eventListController;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.EventListController;
import org.tacs.event_scheduler.api.dtos.event.EventListDTO;
import org.tacs.event_scheduler.domain.entities.EventList;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.services.UserService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NewEventListTest extends AbstractTest {

    @Autowired
    private EventListController eventListController;
    @Autowired
    private UserService userService;

    private String token;

    @Before
    public void setup() {
        this.userService.deleteAll();
        User user = new User();
        user.setName(this.randomString());
        user.setPassword(this.randomString());
        this.userService.save(user);
        this.token = this.userService.login(user.getName(), user.getPassword()).getToken();
    }

    @Test
    public void success() {
        EventListDTO eventListDTO = new EventListDTO();
        eventListDTO.setName(this.randomString());
        this.eventListController.newEventList(this.token, eventListDTO);
        User user = this.userService.getAll().get(0);
        assertEquals(1, user.getEventsList().size());
        EventList eventList = user.getEventsList().get(0);
        assertEquals(eventListDTO.getName(), eventList.getName());
        assertEquals(0, eventList.getEventIds().size());
    }

    @Test
    public void duplicatedName() {
        EventListDTO eventListDTO = new EventListDTO();
        eventListDTO.setName(this.randomString());
        this.eventListController.newEventList(this.token, eventListDTO);
        try {
            this.eventListController.newEventList(this.token, eventListDTO);
            fail();
        } catch (IllegalArgumentException e) {
            User user = this.userService.getAll().get(0);
            assertEquals(1, user.getEventsList().size());
            EventList eventList = user.getEventsList().get(0);
            assertEquals(eventListDTO.getName(), eventList.getName());
            assertEquals(0, eventList.getEventIds().size());
        }
    }


    @Test
    public void emptyName() {
        EventListDTO eventListDTO = new EventListDTO();
        try {
            this.eventListController.newEventList(this.token, eventListDTO);
            fail();
        } catch (IllegalArgumentException e) {
            User user = this.userService.getAll().get(0);
            assertEquals(0, user.getEventsList().size());
        }
    }

}
