package org.tacs.event_scheduler.user.eventController;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestOperations;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.event.EventController;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.repositories.EventbriteRepository;
import org.tacs.event_scheduler.domain.repositories.event.EventResponse;
import org.tacs.event_scheduler.domain.services.EventListService;
import org.tacs.event_scheduler.domain.services.UserService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
public class HistoryTest extends AbstractTest {

	@Autowired
	private EventController eventController;
	@Autowired
	private EventListService eventListService;
	@Autowired
    private UserService userService;
	@Autowired
	private EventbriteRepository eventbriteRepository;
	@Mock
	private RestOperations restOperations;
	
	private User user;
	private long eventListId;
	private final String EVENTBRITE_ID_EVENT_1 = randomString();
	private final String EVENTBRITE_ID_EVENT_2 = randomString();
	
	@Before
	public void setup() {
		this.eventbriteRepository.setRestOperations(this.restOperations);
		when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class))).thenReturn(new ResponseEntity<>(HttpStatus.OK));
		this.userService.deleteAll();
	    user = newUser();   
	    eventListId = newEventList(randomString(), user);
		eventListService.addEvent(EVENTBRITE_ID_EVENT_1, eventListId, user);
		eventListService.addEvent(EVENTBRITE_ID_EVENT_2, eventListId, user);
    }

	@Test
	public void givenTodayFilterWhenTwoEventsWereRegistredTodayThenReturnTwo(){
		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.TODAY);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(2, amount);
	}
	
	@Test
	public void givenTodayFilterWhenOneEventWasRegistredTodayThenReturnOne(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy		
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusDays(1).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.TODAY);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(1, amount);
	}
	
	@Test
	public void givenTodayFilterWhenZeroEventsWereRegistredTodayThenReturnZero(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy		
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusDays(1).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusDays(1).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.TODAY);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(0, amount);
	}
	
	@Test
	public void givenLastThreeDaysFilterWhenTwoEventsWereRegistredTheLastThreeDaysThenReturnTwo(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusDays(1).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusDays(2).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_THREE_DAYS);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(2, amount);
	}
	
	@Test
	public void givenLastThreeDaysFilterWhenOneEventWasRegistredYesterdayThenReturnOne(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusDays(1).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusWeeks(1).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_THREE_DAYS);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(1, amount);
	}
	
	@Test
	public void givenLastThreeDaysFilterWhenZeroEventWereRegistredLastThreeDayThenReturnZero(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusWeeks(2).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusWeeks(1).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_THREE_DAYS);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(0, amount);
	}
	
	@Test
	public void givenLastWeekFilterWhenTwoEventWereRegistredLastWeekThenReturnTwo(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusDays(3).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusDays(4).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_WEEK);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(2, amount);
	}
	
	@Test
	public void givenLastWeekFilterWhenOneEventWasRegistredLastWeekThenReturnOne(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusDays(3).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusMonths(1).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_WEEK);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(1, amount);
	}
	
	@Test
	public void givenLastWeekFilterWhenZeroEventsWereRegistredLastWeekThenReturnZero(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusMonths(3).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusMonths(1).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_WEEK);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(0, amount);
	}
	
	@Test
	public void givenLastMonthFilterWhenTwoEventsWereRegistredLastMonthThenReturnTwo(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusWeeks(3).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusWeeks(2).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_MONTH);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(2, amount);
	}
	
	@Test
	public void givenLastMonthFilterWhenOneEventWasRegistedLastMonthThenReturnOne(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusWeeks(3).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusWeeks(5).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_MONTH);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(1, amount);
	}
	
	@Test
	public void givenLastMonthFilterWhenZeroEventsWereRegistedLastMonthThenReturnZero(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusWeeks(6).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusWeeks(5).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.LAST_MONTH);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(0, amount);
	}
	
	@Test
	public void givenSinceBeginningOfTimeFilterWhenTwoEventsWereRegistredThenReturnsTwo(){
		//Arrange, updateo los eventos para que tengan una fecha posterior a la de hoy
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_1, eventListId, user, DateTime.now().minusYears(5).toDate());
		eventListService.updateDateAddedFromEvent(EVENTBRITE_ID_EVENT_2, eventListId, user, DateTime.now().minusYears(3).toDate());

		//Act
		GenericResponseDTO response = eventController.getHistory(EventListService.SINCE_BEGINNING_OF_TIME);
		int amount = (int) response.getData();
		
		//Assert
		assertEquals(2, amount);
	}
	
    private User newUser() {
        User user = new User();
        user.setName(this.randomString());
        user.setPassword(this.randomString());
        this.userService.save(user);
        return user;
    }
    
    private long newEventList(String eventListName, User user){
    	eventListService.newList(eventListName, user); 
    	return userService.get(user.getId()).getEventsList().get(0).getId();
    }
	
}
