package org.tacs.event_scheduler.user.userController;

import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.UserController;
import org.tacs.event_scheduler.api.dtos.user.LoginDTO;
import org.tacs.event_scheduler.api.dtos.user.UserDTO;
import org.tacs.event_scheduler.domain.entities.user.Role;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.services.UserService;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LoginTest extends AbstractTest {

    @Autowired
    private UserController userController;
    @Autowired
    private UserService userService;

    private UserDTO userDTO = new UserDTO();

    @Before
    public void cleanRepository() {
        this.userService.deleteAll();
        userDTO.setName(this.randomString());
        userDTO.setPassword(this.randomString());
        this.userController.newUser(userDTO);
    }

    @Test
    public void success() {
        DateTime preLogin = DateTime.now();
        LoginDTO loginDTO = (LoginDTO) this.userController.login(this.userDTO).getData();
        assertNotNull(loginDTO);
        User user = this.userService.getByToken(loginDTO.getToken());
        assertFalse(loginDTO.getAdmin());
        assertEquals(userDTO.getName(), user.getName());
        assertNotNull(loginDTO.getToken());
        assertEquals(DigestUtils.sha1Hex(userDTO.getPassword()), user.getPassword());
        assertNotNull(user.getLastAccess());
        assertTrue(user.getLastAccess().getTime() <= DateTime.now().getMillis());
        assertTrue(user.getLastAccess().getTime() >= preLogin.getMillis());
    }

    @Test
    public void successAdmin() {
        DateTime preLogin = DateTime.now();
        User userDB = this.userService.getAll().get(0);
        userDB.setRole(Role.ADMIN);
        this.userService.save(userDB);
        LoginDTO loginDTO = (LoginDTO) this.userController.login(this.userDTO).getData();
        assertNotNull(loginDTO);
        User user = this.userService.getByToken(loginDTO.getToken());
        assertTrue(loginDTO.getAdmin());
        assertEquals(userDTO.getName(), user.getName());
        assertNotNull(loginDTO.getToken());
        assertEquals(DigestUtils.sha1Hex(userDTO.getPassword()), user.getPassword());
        assertNotNull(user.getLastAccess());
        assertTrue(user.getLastAccess().getTime() <= DateTime.now().getMillis());
        assertTrue(user.getLastAccess().getTime() >= preLogin.getMillis());
    }


    @Test
    public void invalidPassword() {
        UserDTO newUserDTO = new UserDTO(this.userDTO.getName(), this.userDTO.getPassword() + "x");
        try {
            this.userController.login(newUserDTO);
            fail();
        } catch (IllegalArgumentException e) {
            User user = this.userService.getAll().get(0);
            assertEquals(userDTO.getName(), user.getName());
            assertEquals(DigestUtils.sha1Hex(userDTO.getPassword()), user.getPassword());
            assertNull(user.getLastAccess());
        }
    }

    @Test
    public void invalidUsername() {
        UserDTO newUserDTO = new UserDTO(this.userDTO.getName() + "X", this.userDTO.getPassword());
        try {
            this.userController.login(newUserDTO);
            fail();
        } catch (IllegalArgumentException e) {
            User user = this.userService.getAll().get(0);
            assertEquals(userDTO.getName(), user.getName());
            assertEquals(DigestUtils.sha1Hex(userDTO.getPassword()), user.getPassword());
            assertNull(user.getLastAccess());
        }
    }
}
