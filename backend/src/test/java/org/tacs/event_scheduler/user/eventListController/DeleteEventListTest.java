package org.tacs.event_scheduler.user.eventListController;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.EventListController;
import org.tacs.event_scheduler.api.dtos.event.EventListDTO;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.services.UserService;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DeleteEventListTest extends AbstractTest {

    @Autowired
    private EventListController eventListController;
    @Autowired
    private UserService userService;

    private final String NAME = this.randomString();
    private String token;

    @Before
    public void setup() {
        this.userService.deleteAll();
        this.newUser();
        this.newEvent();
    }

    private void newUser() {
        User user = new User();
        user.setName(this.randomString());
        user.setPassword(this.randomString());
        this.userService.save(user);
        this.token = userService.login(user.getName(), user.getPassword()).getToken();
    }

    private void newEvent() {
        EventListDTO eventListDTO = new EventListDTO();
        eventListDTO.setName(NAME);
        this.eventListController.newEventList(this.token, eventListDTO);
    }

    @Test
    public void success() {
        User user = this.userService.getAll().get(0);
        this.eventListController.deleteEventList(this.token, user.getEventsList().get(0).getId());
        user = this.userService.getAll().get(0);
        assertEquals(0,user.getEventsList().size());
    }

    @Test
    public void failDelete() {
        User user = this.userService.getAll().get(0);
        try {
            this.eventListController.deleteEventList(this.token,user.getEventsList().get(0).getId() + 1);
            fail();
        } catch (IllegalArgumentException e) {
            user = this.userService.getAll().get(0);
            assertEquals(1, user.getEventsList().size());
            assertEquals(NAME, user.getEventsList().get(0).getName());
        }
    }
}
