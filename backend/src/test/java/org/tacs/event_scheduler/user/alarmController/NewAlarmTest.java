package org.tacs.event_scheduler.user.alarmController;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.AlarmController;
import org.tacs.event_scheduler.api.dtos.AlarmDTO;
import org.tacs.event_scheduler.api.dtos.event.search.DateDTO;
import org.tacs.event_scheduler.api.dtos.event.search.LocationDTO;
import org.tacs.event_scheduler.api.dtos.event.search.SearchCriteriaDTO;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alarm;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NewAlarmTest extends AbstractTest {

    @Autowired
    private AlarmController alarmController;
    @Autowired
    private UserService userService;

    private String token;

    @Before
    public void cleanRepository() {
        this.userService.deleteAll();
        User user = new User();
        user.setName(this.randomString());
        user.setPassword(this.randomString());
        this.userService.save(user);
        this.token = this.userService.login(user.getName(), user.getPassword()).getToken();
    }

    @Test
    public void success() {
        AlarmDTO alarmDTO = new AlarmDTO();
        alarmDTO.setPrice(this.randomString());
        alarmDTO.setKeyword(this.randomString());
        alarmDTO.setDate_end(this.randomDate().getMillis());
        alarmDTO.setDate_start(this.randomDate().getMillis());
        alarmDTO.setLocation_address(this.randomString());
        alarmDTO.setLocation_distance(this.randomInteger(40));
        alarmDTO.setName(this.randomString());

        this.alarmController.newAlarm(this.token, alarmDTO);

        List<User> userList = this.userService.getAll();
        assertEquals(1, userList.size());

        assertEquals(1, userList.get(0).getAlarms().size());

        Alarm alarm = userList.get(0).getAlarms().get(0);
        assertEquals(alarmDTO.getName(), alarm.getName());
        assertNotNull(alarm.getCriteria());

        assertEquals(alarmDTO.getKeyword(), alarm.getCriteria().getKeyword());
    }
}
