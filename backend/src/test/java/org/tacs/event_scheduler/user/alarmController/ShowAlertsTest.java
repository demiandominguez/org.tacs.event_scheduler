package org.tacs.event_scheduler.user.alarmController;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestOperations;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.AlarmController;
import org.tacs.event_scheduler.api.dtos.AlertDTO;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alarm;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alert;
import org.tacs.event_scheduler.domain.repositories.EventbriteRepository;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ShowAlertsTest extends AbstractTest {

    @Autowired
    private AlarmController alarmController;
    @Autowired
    private UserService userService;
    @Autowired
    private EventbriteRepository eventbriteRepository;
    @Mock
    private RestOperations restOperations;

    private User user;
    private final String TOKEN = this.randomString();

    @Before
    public void cleanRepository() {
        this.eventbriteRepository.setRestOperations(this.restOperations);
        this.userService.deleteAll();
        this.user = new User();
        this.user.setName(this.randomString());
        this.user.setPassword(this.randomString());
        this.user.setToken(TOKEN);
        this.userService.save(this.user);
    }

    @Test
    public void success() {
        Alarm alarm1 = new Alarm();
        alarm1.setName("Alarm 1");
        alarm1.setLastResult(Lists.newArrayList(this.randomAlert(), this.randomAlert(), this.randomAlert()));

        Alarm alarm2 = new Alarm();
        alarm2.setName("Alarm 2");
        alarm2.setLastResult(Lists.newArrayList(this.randomAlert(), this.randomAlert()));

        Alarm alarm3 = new Alarm();
        alarm3.setName("Alarm 3");

        this.user.setAlarms(Lists.newArrayList(alarm1, alarm2, alarm3));
        this.userService.save(user);

        Map<String, List<AlertDTO>> response = (Map<String, List<AlertDTO>>) this.alarmController.getAlerts(TOKEN).getData();
        assertEquals(3, response.keySet().size());
        assertNotNull(response.get("Alarm 1"));
        assertEquals(3, response.get("Alarm 1").size());


        assertNotNull(response.get("Alarm 2"));
        assertEquals(2, response.get("Alarm 2").size());

        assertNotNull(response.get("Alarm 3"));
        assertEquals(0, response.get("Alarm 3").size());
    }

    private Alert randomAlert() {
        Alert alert = new Alert();
        alert.setUrl(this.randomString());
        alert.setEventbriteId(this.randomString());
        alert.setName(this.randomString());
        return alert;
    }

    @Test
    public void emptyAlert() {

        Alarm alarm1 = new Alarm();
        alarm1.setName("Alarm 1");

        Alarm alarm2 = new Alarm();
        alarm2.setName("Alarm 2");

        this.user.setAlarms(Lists.newArrayList(alarm1, alarm2));
        this.userService.save(user);

        Map<String, List<AlertDTO>> response = (Map<String, List<AlertDTO>>) this.alarmController.getAlerts(TOKEN).getData();
        assertEquals(2, response.keySet().size());
        assertNotNull(response.get("Alarm 1"));
        assertEquals(0, response.get("Alarm 1").size());


        assertNotNull(response.get("Alarm 2"));
        assertEquals(0, response.get("Alarm 2").size());
    }
}
