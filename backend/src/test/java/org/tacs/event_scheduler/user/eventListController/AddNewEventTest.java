package org.tacs.event_scheduler.user.eventListController;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestOperations;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.EventListController;
import org.tacs.event_scheduler.api.dtos.user.AddEventDTO;
import org.tacs.event_scheduler.domain.entities.EventList;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.repositories.EventbriteRepository;
import org.tacs.event_scheduler.domain.repositories.event.EventResponse;
import org.tacs.event_scheduler.domain.services.UserService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AddNewEventTest extends AbstractTest {

    @Autowired
    private EventListController eventListController;
    @Autowired
    private EventbriteRepository eventbriteRepository;
    @Autowired
    private UserService userService;
    @Mock
    private RestOperations restOperations;
    
    private final String TOKEN = this.randomString();

    @Before
    public void setup() {
        this.eventbriteRepository.setRestOperations(this.restOperations);
        this.userService.deleteAll();
        this.newUser();
    }

    private void newUser() {
        User user = new User();
        user.setName(this.randomString());
        user.setPassword(this.randomString());
        user.setToken(TOKEN);
        EventList eventList = new EventList();
        eventList.setName(this.randomString());
        user.setEventsList(Lists.newArrayList(eventList));
        this.userService.save(user);
    }

    @Test
    public void success() {
        String eventId = this.randomString();
        Mockito.when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class))).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        this.eventListController.addNewEvent(TOKEN, this.getDTO(eventId));
        EventList eventList = this.userService.getAll().get(0).getEventsList().get(0);
        assertEquals(1, eventList.getEventIds().size());
        assertEquals(eventId, eventList.getEventIds().get(0));
        Mockito.verify(this.restOperations).exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class));
    }

    @Test
    public void duplicateEvent() {
        String eventId = this.randomString();
        Mockito.when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class))).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        this.eventListController.addNewEvent(TOKEN, this.getDTO(eventId));
        this.eventListController.addNewEvent(TOKEN, this.getDTO(eventId));
        EventList eventList = this.userService.getAll().get(0).getEventsList().get(0);
        assertEquals(1, eventList.getEventIds().size());
        assertEquals(eventId, eventList.getEventIds().get(0));

        Mockito.verify(this.restOperations, times(1)).exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class));

    }

    private AddEventDTO getDTO(String eventId) {
        AddEventDTO addEventDTO = new AddEventDTO();
        addEventDTO.setEventId(eventId);
        addEventDTO.setEventListId(this.userService.getAll().get(0).getEventsList().get(0).getId());
        return addEventDTO;
    }


    @Test
    public void eventNotFound() {
        String eventId = this.randomString();
        Mockito.when(this.restOperations.exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class))).thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));
        try {
            this.eventListController.addNewEvent(TOKEN, this.getDTO(eventId));
            fail();
        } catch (IllegalArgumentException e) {
            EventList eventList = this.userService.getAll().get(0).getEventsList().get(0);
            assertEquals(0, eventList.getEventIds().size());
            Mockito.verify(this.restOperations).exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class));
        }
    }


    @Test
    public void eventListNotFound() {
        String eventId = this.randomString();
        AddEventDTO addEventDTO = new AddEventDTO();
        addEventDTO.setEventId(eventId);
        addEventDTO.setEventListId((long) -1);
        try {
            this.eventListController.addNewEvent(TOKEN, addEventDTO);
            fail();
        } catch (IllegalArgumentException e) {
            Mockito.verify(this.restOperations, never()).exchange(anyString(), eq(HttpMethod.GET), any(), eq(EventResponse.class));
        }
    }
}
