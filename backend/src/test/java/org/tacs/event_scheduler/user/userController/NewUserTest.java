package org.tacs.event_scheduler.user.userController;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.tacs.event_scheduler.AbstractTest;
import org.tacs.event_scheduler.api.controllers.user.UserController;
import org.tacs.event_scheduler.api.dtos.user.UserDTO;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NewUserTest extends AbstractTest {

    @Autowired
    private UserController userController;
    @Autowired
    private UserService userService;

    @Before
    public void cleanRepository() {
        this.userService.deleteAll();
        assertEquals(0,this.userService.getAll().size());
    }

    @Test
    public void success() {
        UserDTO userDTO = new UserDTO();
        userDTO.setName(this.randomString());
        userDTO.setPassword(this.randomString());

        this.userController.newUser(userDTO);
        List<User> userList = this.userService.getAll();

        assertEquals(1, userList.size());

        User userTest = userList.get(0);

        assertEquals(userDTO.getName(), userTest.getName());
        assertEquals(DigestUtils.sha1Hex(userDTO.getPassword()), userTest.getPassword());

        assertEquals(0, userTest.getAlarms().size());
        assertEquals(0, userTest.getEventsList().size());
    }

    @Test
    public void duplicateUsernameTest() {
        String NAME = this.randomString();
        String PASSWORD = this.randomString();
        UserDTO userDTO = new UserDTO(NAME, PASSWORD);
        String OTHER_PASSWORD = this.randomString();
        UserDTO duplicatedUserDTO = new UserDTO(NAME, OTHER_PASSWORD);

        this.userController.newUser(userDTO);
        try {
            this.userController.newUser(duplicatedUserDTO);
            fail();
        } catch (IllegalArgumentException e) {
            List<User> userList = this.userService.getAll();
            assertEquals(1, userList.size());
            User userTest = userList.get(0);
            assertEquals(NAME, userTest.getName());
            assertEquals(DigestUtils.sha1Hex(PASSWORD), userTest.getPassword());
            assertEquals(0,userTest.getAlarms().size());
            assertEquals(0, userTest.getEventsList().size());
        }
    }

    @Test
    public void withoutNameTest() {
        UserDTO userDTO = new UserDTO(null, this.randomString());
        try {
            this.userController.newUser(userDTO);
            fail();
        } catch (IllegalArgumentException e) {
            List<User> userList = this.userService.getAll();
            assertEquals(0, userList.size());
        }
    }

    @Test
    public void withoutPasswordTest() {
        UserDTO userDTO = new UserDTO(this.randomString(), null);
        try {
            this.userController.newUser(userDTO);
            fail();
        } catch (IllegalArgumentException e) {
            List<User> userList = this.userService.getAll();
            assertEquals(0, userList.size());
        }
    }

}
