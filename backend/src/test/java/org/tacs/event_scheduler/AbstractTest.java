package org.tacs.event_scheduler;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.joda.time.DateTime;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public abstract class AbstractTest {

    protected String randomString() {
        return UUID.randomUUID().toString();
    }

    protected List<Integer> randomIntegerList() {
        Integer size = this.randomInteger(10);
        List<Integer> list = Lists.newArrayList();
        for (int i = 0; i < size; i++)
            list.add(this.randomInteger(100));
        return list;
    }

    protected Integer randomInteger(int limit) {
        return (int) (Math.random() * limit) + 1;
    }

    protected DateTime randomDate() {
        return new DateTime().withDayOfYear(this.randomInteger(365));
    }

    protected List<String> randomStringList() {
        Integer size = this.randomInteger(10);
        List<String> list = Lists.newArrayList();
        for (int i = 0; i < size; i++)
            list.add(this.randomString());
        return list;
    }

    protected <T> T readDTO(String fileName, Class<T> tClass) {
        fileName = "/test/" + fileName;
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        try {
            FileReader fr = new FileReader(getClass().getResource(fileName).getFile());
            JsonElement data = parser.parse(fr);
            fr.close();
            return gson.fromJson(data.toString(), tClass);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
