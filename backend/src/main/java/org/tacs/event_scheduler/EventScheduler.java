package org.tacs.event_scheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan
@EntityScan
@EnableScheduling
public class EventScheduler {

    public static void main(String[] args) {
        SpringApplication.run(EventScheduler.class, args);
    }
}
