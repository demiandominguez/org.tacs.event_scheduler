package org.tacs.event_scheduler.api.controllers.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tacs.event_scheduler.api.controllers.AbstractController;
import org.tacs.event_scheduler.api.dtos.EventDTO;
import org.tacs.event_scheduler.api.dtos.FilterDTO;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;
import org.tacs.event_scheduler.api.transformers.EventTransformer;
import org.tacs.event_scheduler.api.transformers.FilterTransformer;
import org.tacs.event_scheduler.domain.entities.Eventbrite;
import org.tacs.event_scheduler.domain.entities.category.Category;
import org.tacs.event_scheduler.domain.entities.user.alarm.SearchCriteria;
import org.tacs.event_scheduler.domain.services.CategoryService;
import org.tacs.event_scheduler.domain.services.EventListService;
import org.tacs.event_scheduler.domain.services.EventService;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;

@Controller
@RequestMapping("/events")
public class EventController extends AbstractController {

    @Autowired
    private UserService userService;
    @Autowired
    private EventListService eventListService;
    @Autowired
    private EventTransformer eventTransformer;
    @Autowired
    private EventService eventService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private FilterTransformer filterTransformer;
    
    
    @RequestMapping()
	@ResponseBody
	public GenericResponseDTO getEvents(
			@RequestHeader String token,
			@RequestParam(name = "keyword", required = false) String keyword,
			@RequestParam(name = "categories", required = false) List<String> categories,
			@RequestParam(name = "subcategories", required = false) List<String> subcategories,
			@RequestParam(name = "price", required = false) String price,
			@RequestParam(name = "sortBy", required = false) String sortBy,
			@RequestParam(name = "page", required = false) Integer page,
            @RequestParam(value = "date_start", required = false) Long startDate,
            @RequestParam(value = "date_end", required = false) Long endDate,
            @RequestParam(value = "maxQuantity", required = false) Integer maxQuantity) {
        this.userService.validateUser(token);
		SearchCriteria searchCriteria = this.filterTransformer.transform(keyword,categories, subcategories, price, sortBy, page, startDate, endDate);
		List<Eventbrite> baseEvents = this.eventService.get(searchCriteria);
		if(maxQuantity != null) {
			baseEvents = baseEvents.subList(0, maxQuantity);
		}

		List<EventDTO> eventDTOS = this.eventTransformer.transform(baseEvents);
		return new GenericResponseDTO("OK", eventDTOS);
	}
	
    @RequestMapping("/history")
    @ResponseBody
    public GenericResponseDTO getHistory(@RequestParam String filterBy) {
        int amount = eventListService.getAmountOfEventsByHistoryCriteria(filterBy);
        return new GenericResponseDTO("OK", amount);
    }

    @RequestMapping("/filters")
    @ResponseBody
    public GenericResponseDTO getFilters(@RequestHeader String token ) {
        this.userService.validateUser(token);
        List<Category> categories = this.categoryService.getCategoriesAndSubCategories();
        FilterDTO filterDTO = this.filterTransformer.transform(categories);
        return new GenericResponseDTO("OK", filterDTO);
    }

}
