package org.tacs.event_scheduler.api.dtos.event.search;

import org.tacs.event_scheduler.domain.entities.AbstractEntity;

public class LocationDTO extends AbstractEntity{

    private String address;
    private Integer distance;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
}
