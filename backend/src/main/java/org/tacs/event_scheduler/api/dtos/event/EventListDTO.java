package org.tacs.event_scheduler.api.dtos.event;

import org.tacs.event_scheduler.api.dtos.EventDTO;

import java.util.List;

public class EventListDTO {

    private Long id;
    private String name;
    private List<EventDTO> events;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EventDTO> getEvents() {
        return events;
    }

    public void setEvents(List<EventDTO> events) {
        this.events = events;
    }
}
