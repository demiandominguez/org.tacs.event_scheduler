package org.tacs.event_scheduler.api.controllers.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.tacs.event_scheduler.api.controllers.AbstractController;
import org.tacs.event_scheduler.api.dtos.AlarmDTO;
import org.tacs.event_scheduler.api.dtos.AlertDTO;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;
import org.tacs.event_scheduler.api.transformers.AlarmTransformer;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alarm;
import org.tacs.event_scheduler.domain.services.AlarmService;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/users/alarms")
public class AlarmController extends AbstractController {

    @Autowired
    private AlarmService alarmService;
    @Autowired
    private AlarmTransformer alarmTransformer;
    @Autowired
    private UserService userService;
 
    @RequestMapping()
    @ResponseBody
    public GenericResponseDTO getAlarms(@RequestHeader String token) {
    	List<Alarm> alarms = this.userService.getByToken(token).getAlarms();
    	return new GenericResponseDTO("OK", alarms);
    }

    @RequestMapping("/alerts")
    @ResponseBody
    public GenericResponseDTO getAlerts(@RequestHeader String token) {
        User user = this.userService.getByToken(token);
        Map<String, List<AlertDTO>> alertDTOMap = this.alarmTransformer.transformToAlertDTO(user);
        return new GenericResponseDTO("OK", alertDTOMap);
    }

    @RequestMapping(method = POST)
    @ResponseBody
    @Transactional
    public GenericResponseDTO newAlarm(@RequestHeader String token, @RequestBody AlarmDTO alarmDTO) {
        Alarm alarm = this.alarmTransformer.transform(alarmDTO);
        this.alarmService.save(alarm,this.userService.getByToken(token));
        return OK;
    }
}
