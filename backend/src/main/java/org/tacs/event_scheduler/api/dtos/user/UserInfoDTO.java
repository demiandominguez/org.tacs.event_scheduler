package org.tacs.event_scheduler.api.dtos.user;

import java.util.Date;

public class UserInfoDTO {

	private long id;
	private String name;
	private Integer listsAmount;
	private Integer alarmsAmount;
	private Date lastAccess;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getListsAmount() {
		return listsAmount;
	}

	public void setListsAmount(Integer listsAmount) {
		this.listsAmount = listsAmount;
	}

	public Integer getAlarmsAmount() {
		return alarmsAmount;
	}

	public void setAlarmsAmount(Integer alarmsAmount) {
		this.alarmsAmount = alarmsAmount;
	}

	public Date getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(Date lastAccess) {
		this.lastAccess = lastAccess;
	}
}
