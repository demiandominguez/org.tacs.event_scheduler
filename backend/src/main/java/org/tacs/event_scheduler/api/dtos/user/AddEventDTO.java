package org.tacs.event_scheduler.api.dtos.user;

public class AddEventDTO {

    private String eventId;
    private Long eventListId;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Long getEventListId() {
        return eventListId;
    }

    public void setEventListId(Long eventListId) {
        this.eventListId = eventListId;
    }
}
