package org.tacs.event_scheduler.api.transformers;

import org.springframework.stereotype.Component;
import org.tacs.event_scheduler.api.dtos.CategoryDTO;
import org.tacs.event_scheduler.api.dtos.FilterDTO;
import org.tacs.event_scheduler.domain.entities.category.Category;
import org.tacs.event_scheduler.domain.entities.category.CategoryEnum;
import org.tacs.event_scheduler.domain.entities.user.alarm.SearchCriteria;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FilterTransformer {

    public SearchCriteria transform(String keyword, List<String> categories, List<String> subcategories, String price, String sortBy,
    								Integer page, Long startDate, Long endDate) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setKeyword(keyword);
        searchCriteria.setCategories(categories);
        searchCriteria.setSubcategories(subcategories);
        searchCriteria.setPage(page);
        searchCriteria.setSortBy(sortBy);
        searchCriteria.setPrice(price);
        searchCriteria.setStartDate(startDate);
        searchCriteria.setEndDate(endDate);
        return searchCriteria;
    }

    public FilterDTO transform(List<Category> categories) {
        FilterDTO filterDTO = new FilterDTO();
        filterDTO.setCategories(categories
                .stream()
                .filter(category -> category.getType().equals(CategoryEnum.MAIN))
                .map(this::transformToCategoryDTO)
                .collect(Collectors.toList()));

        filterDTO.setSubcategories(categories
                .stream()
                .filter(category -> category.getType().equals(CategoryEnum.SUB))
                .map(this::transformToCategoryDTO)
                .collect(Collectors.toList()));
        return filterDTO;
    }

    private CategoryDTO transformToCategoryDTO(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getEventbriteId());
        categoryDTO.setName(category.getName());
        return categoryDTO;
    }
}
