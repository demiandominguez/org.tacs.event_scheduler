package org.tacs.event_scheduler.api.dtos;

import java.util.List;

public class FilterDTO {

    private List<CategoryDTO> categories;
    private List<CategoryDTO> subcategories;

    public List<CategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDTO> categories) {
        this.categories = categories;
    }

    public List<CategoryDTO> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<CategoryDTO> subcategories) {
        this.subcategories = subcategories;
    }
}
