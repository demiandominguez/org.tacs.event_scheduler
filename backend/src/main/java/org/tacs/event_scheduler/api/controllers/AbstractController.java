package org.tacs.event_scheduler.api.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;

@CrossOrigin(origins = "*")
public abstract class AbstractController {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	protected final GenericResponseDTO OK = new GenericResponseDTO("OK");

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public GenericResponseDTO expectedException(IllegalArgumentException e) {
		LOGGER.warn("Illegal argument exception has been produced: {}", e.getMessage());
		return new GenericResponseDTO();
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public GenericResponseDTO unexpectedException(Exception e) {
		LOGGER.error("Unexpected exception has been produced: {}", e);
		return new GenericResponseDTO();
	}
}
