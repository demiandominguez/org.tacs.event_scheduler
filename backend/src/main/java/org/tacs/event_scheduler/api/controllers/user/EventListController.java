package org.tacs.event_scheduler.api.controllers.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.tacs.event_scheduler.api.controllers.AbstractController;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;
import org.tacs.event_scheduler.api.dtos.event.EventListDTO;
import org.tacs.event_scheduler.api.dtos.user.AddEventDTO;
import org.tacs.event_scheduler.api.transformers.EventTransformer;
import org.tacs.event_scheduler.domain.entities.EventList;
import org.tacs.event_scheduler.domain.entities.Eventbrite;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.services.EventListService;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping("/users/event-lists")
public class EventListController extends AbstractController{

    @Autowired
    private EventListService eventListService;
    @Autowired
    private UserService userService;
    @Autowired
    private EventTransformer eventTransformer;

    @RequestMapping(method = POST)
    @ResponseBody
    @Transactional
    public GenericResponseDTO newEventList(@RequestHeader String token, @RequestBody EventListDTO eventListDTO) {
        this.userService.validateUser(token);
        this.eventListService.newList(eventListDTO.getName(), this.userService.getByToken(token));
        return OK;
    }

    @RequestMapping(method = PUT)
    @ResponseBody
    @Transactional
    public GenericResponseDTO editEventList(@RequestHeader String token, @RequestBody EventListDTO eventListDTO) {
        this.userService.validateUser(token);
        this.eventListService.update(eventListDTO.getId(), eventListDTO.getName(), this.userService.getByToken(token));
        return OK;
    }

    @RequestMapping(method = DELETE)
    @ResponseBody
    @Transactional
    public GenericResponseDTO deleteEventList(@RequestHeader String token, @RequestParam Long id) {
        this.userService.validateUser(token);
        this.eventListService.delete(id, this.userService.getByToken(token));
        return OK;
    }

    @RequestMapping(value="/events", method = DELETE)
    @ResponseBody
    @Transactional
    public GenericResponseDTO deleteEventOfList(@RequestHeader String token, @RequestParam Long id, @RequestParam Long eventId) {
        User user = this.userService.getByToken(token);
        this.eventListService.deleteEvent(id,eventId, user);
        return OK;
    }

    @RequestMapping(value = "/event",method = POST)
    @ResponseBody
    @Transactional
    public GenericResponseDTO addNewEvent(@RequestHeader String token, @RequestBody AddEventDTO eventDTO) {
        this.userService.validateUser(token);
    	this.eventListService.addEvent(eventDTO.getEventId(), eventDTO.getEventListId(), this.userService.getByToken(token));
        return OK;
    }
    
    @RequestMapping()
    @ResponseBody
    public GenericResponseDTO getEventLists(@RequestHeader String token) {
    	User user = this.userService.getByToken(token);
        Map<EventList, List<Eventbrite>> lists = this.eventListService.getEvents(user);
        List<EventListDTO> dto = this.eventTransformer.transform(lists);
        return new GenericResponseDTO("OK", dto);
    }
    
    @RequestMapping("/interested")
    @ResponseBody
    public GenericResponseDTO searchInterested(@RequestHeader String token, @RequestParam String eventId) {
        this.userService.validateAdmin(token);
      	int amount = userService.getAmountOfUsersInterestedInEvent(eventId);
    	return new GenericResponseDTO("OK", amount);
    }
}
