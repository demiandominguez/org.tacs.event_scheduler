package org.tacs.event_scheduler.api.controllers.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.tacs.event_scheduler.api.controllers.AbstractController;
import org.tacs.event_scheduler.api.dtos.EventDTO;
import org.tacs.event_scheduler.api.dtos.GenericResponseDTO;
import org.tacs.event_scheduler.api.dtos.user.LoginDTO;
import org.tacs.event_scheduler.api.dtos.user.UserDTO;
import org.tacs.event_scheduler.api.dtos.user.UserInfoDTO;
import org.tacs.event_scheduler.api.transformers.EventTransformer;
import org.tacs.event_scheduler.api.transformers.UserTransformer;
import org.tacs.event_scheduler.domain.entities.Eventbrite;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.services.EventService;
import org.tacs.event_scheduler.domain.services.UserService;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/users")
public class UserController extends AbstractController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserTransformer userTransformer;
    @Autowired
    private EventService eventService;
    @Autowired
    private EventTransformer eventTransformer;

    @RequestMapping(method = POST)
    @ResponseBody
    @Transactional
    public GenericResponseDTO newUser(@RequestBody UserDTO userDTO) {
        User user = this.userTransformer.transform(userDTO);
        this.userService.save(user);
        return OK;
    }

    @RequestMapping(path = "/login", method = POST)
    @ResponseBody
    @Transactional
    public GenericResponseDTO login(@RequestBody UserDTO userDTO) {
        User user = this.userService.login(userDTO.getName(), DigestUtils.sha1Hex(userDTO.getPassword()));
        LoginDTO loginDTO = this.userTransformer.transformToLoginDTO(user);
        return new GenericResponseDTO("OK", loginDTO);
    }

    @RequestMapping(path = "/logout", method = POST)
    @ResponseBody
    @Transactional
    public GenericResponseDTO logout(@RequestHeader String token) {
        try {
            this.userService.logout(token);
        } catch (IllegalArgumentException ignored) {}
        return OK;
    }
    
    @RequestMapping(path = "/all")
    @ResponseBody
    public GenericResponseDTO getAllUser(@RequestHeader String token) {
        this.userService.validateAdmin(token);
        List<User> users = userService.getAll();
        List<UserInfoDTO> dtos = users.stream()
        							  .map(user -> userTransformer.transform(user))
        							  .collect(Collectors.toList());
        
        return new GenericResponseDTO("OK", dtos);
    }

    @RequestMapping()
    @ResponseBody
    public GenericResponseDTO getUser(@RequestHeader String token, @RequestParam Long id) {
        this.userService.validateAdmin(token);
        User user = this.userService.get(id);
        UserInfoDTO dto = this.userTransformer.transform(user);
        return new GenericResponseDTO("OK", dto);
    }

    @RequestMapping("/comparisons")
    @ResponseBody
    public GenericResponseDTO compare(@RequestHeader String token, @RequestParam Long id, @RequestParam Long otherId) {
        this.userService.validateAdmin(token);
        List<String> commonEventsIds = this.userService.compare(id, otherId);
        List<Eventbrite> events = this.eventService.getByIds(commonEventsIds);
        List<EventDTO> eventDTOs = this.eventTransformer.transform(events);
        return new GenericResponseDTO("OK", eventDTOs);
    }
}
