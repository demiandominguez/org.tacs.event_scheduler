package org.tacs.event_scheduler.api.dtos.user;

public class LoginDTO {

    private String token;
    private Boolean isAdmin;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }
}
