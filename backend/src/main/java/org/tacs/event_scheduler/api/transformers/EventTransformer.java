package org.tacs.event_scheduler.api.transformers;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.tacs.event_scheduler.api.dtos.EventDTO;
import org.tacs.event_scheduler.api.dtos.event.EventListDTO;
import org.tacs.event_scheduler.domain.entities.EventList;
import org.tacs.event_scheduler.domain.entities.Eventbrite;

import com.google.common.collect.Lists;

@Component
public class EventTransformer {
    public List<EventDTO> transform(List<Eventbrite> baseEvents) {
        return baseEvents
                .stream()
                .map(baseEvent -> {
                    EventDTO eventDTO = new EventDTO();
                    if (baseEvent.getCategory() != null)
                        eventDTO.setCategory(baseEvent.getCategory().getName());
                    if (baseEvent.getSubCategory() != null)
                        eventDTO.setSubcategory(baseEvent.getSubCategory().getName());
                    eventDTO.setUrl(baseEvent.getUrl());
                    eventDTO.setStartDate(baseEvent.getStartDate().getMillis());
                    eventDTO.setEndDate(baseEvent.getEndDate().getMillis());
                    eventDTO.setName(baseEvent.getName());
                    eventDTO.setId(baseEvent.getId());
                    eventDTO.setFree(baseEvent.getFree());
                    return eventDTO;
                }).collect(Collectors.toList());
    }

    public List<EventListDTO> transform(Map<EventList, List<Eventbrite>> lists) {
        List<EventListDTO> dto = Lists.newArrayList();
        for (Map.Entry<EventList, List<Eventbrite>> entry : lists.entrySet()) {
            EventListDTO eventListDTO = new EventListDTO();
            EventList eventList = entry.getKey();
            eventListDTO.setId(eventList.getId());
            eventListDTO.setName(eventList.getName());
            eventListDTO.setEvents(this.transform(entry.getValue()));
            dto.add(eventListDTO);
        }
        return dto;
    }
}
