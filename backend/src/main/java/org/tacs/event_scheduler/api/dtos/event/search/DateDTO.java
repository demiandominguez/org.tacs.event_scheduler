package org.tacs.event_scheduler.api.dtos.event.search;

import org.joda.time.DateTime;

public class DateDTO {

    private DateTime start;
    private DateTime end;
    private String keyword;

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
