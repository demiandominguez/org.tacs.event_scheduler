package org.tacs.event_scheduler.api.dtos;

import java.util.List;

public class AlarmDTO {

	private Long id;
	private String name;
	private String keyword;
	private int location_distance;
	private String location_address;
	private List<String> categories;
	private List<String> subcategories;
	private String price;
	private Long date_start;
	private Long date_end;
	private String date_keyword;
	private String sortBy;
	
	
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getLocation_distance() {
		return location_distance;
	}

	public void setLocation_distance(int location_distance) {
		this.location_distance = location_distance;
	}

	public String getLocation_address() {
		return location_address;
	}

	public void setLocation_address(String location_address) {
		this.location_address = location_address;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public List<String> getSubcategories() {
		return subcategories;
	}

	public void setSubcategories(List<String> subcategories) {
		this.subcategories = subcategories;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Long getDate_start() {
		return date_start;
	}

	public void setDate_start(Long date_start) {
		this.date_start = date_start;
	}

	public Long getDate_end() {
		return date_end;
	}

	public void setDate_end(Long date_end) {
		this.date_end = date_end;
	}

	public String getDate_keyword() {
		return date_keyword;
	}

	public void setDate_keyword(String date_keyword) {
		this.date_keyword = date_keyword;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
