package org.tacs.event_scheduler.api.transformers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tacs.event_scheduler.api.dtos.AlarmDTO;
import org.tacs.event_scheduler.api.dtos.AlertDTO;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alarm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class AlarmTransformer {

    @Autowired
    private FilterTransformer filterTransformer;

    public Alarm transform(AlarmDTO alarmDTO) {
        Alarm alarm = new Alarm();
        alarm.setName(alarmDTO.getName());
        alarm.setCriteria(
                this.filterTransformer.transform(
                        alarmDTO.getKeyword(),
                        alarmDTO.getCategories(),
                        alarmDTO.getSubcategories(),
                        alarmDTO.getPrice(),
                        alarmDTO.getSortBy(),
                        null,
                        alarmDTO.getDate_start(),
                        alarmDTO.getDate_end()
                )
        );
        return alarm;
    }

    public List<Alarm> transform(List<AlarmDTO> alarmDTOs) {
        List<Alarm> alarms = new ArrayList<>();
        for (AlarmDTO alarmDTO : alarmDTOs) {
            alarms.add(this.transform(alarmDTO));
        }
        return alarms;
    }

    public Map<String, List<AlertDTO>> transformToAlertDTO(User user) {
        return  user.getAlarms()
                .stream()
                .collect(Collectors.toMap(Alarm::getName, alarm -> alarm.getLastResult()
                        .stream()
                        .map(alert -> {
                            AlertDTO alertDTO = new AlertDTO();
                            alertDTO.setName(alert.getName());
                            alertDTO.setUrl(alert.getUrl());
                            return alertDTO;
                        }).collect(Collectors.toList())));

    }
}
