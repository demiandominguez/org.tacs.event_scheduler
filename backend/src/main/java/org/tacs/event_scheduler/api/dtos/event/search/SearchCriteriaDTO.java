package org.tacs.event_scheduler.api.dtos.event.search;

import java.util.List;

public class SearchCriteriaDTO {

    private String keyword;
    private List<String> categories;
    private List<String>  subcategories;
    private String price;
    private DateDTO date;
    private LocationDTO locationDTO;

    public SearchCriteriaDTO(String keyword, String address, Integer distance, List<String> categories, List<String> subcategories, String price) {
        this.keyword = keyword;
        LocationDTO locationDTO = new LocationDTO();
        locationDTO.setAddress(address);
        locationDTO.setDistance(distance);

        this.categories = categories;
        this.subcategories = subcategories;
        this.price = price;
    }


    public SearchCriteriaDTO() {
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<String> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<String> subcategories) {
        this.subcategories = subcategories;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public DateDTO getDate() {
        return date;
    }

    public void setDate(DateDTO date) {
        this.date = date;
    }

	public LocationDTO getLocationDTO() {
		return locationDTO;
	}


	public void setLocationDTO(LocationDTO locationDTO) {
		this.locationDTO = locationDTO;
	}
}
