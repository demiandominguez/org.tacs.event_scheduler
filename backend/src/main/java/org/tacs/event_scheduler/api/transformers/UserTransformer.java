package org.tacs.event_scheduler.api.transformers;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.tacs.event_scheduler.api.dtos.user.LoginDTO;
import org.tacs.event_scheduler.api.dtos.user.UserDTO;
import org.tacs.event_scheduler.api.dtos.user.UserInfoDTO;
import org.tacs.event_scheduler.domain.entities.user.Role;
import org.tacs.event_scheduler.domain.entities.user.User;

@Component
public class UserTransformer {

    public User transform(UserDTO userDTO) {
        this.validate(userDTO);
        User user = new User();
        user.setName(userDTO.getName());
        user.setPassword(DigestUtils.sha1Hex(userDTO.getPassword()));
        return user;
    }

    private void validate(UserDTO userDTO) {
        if (Strings.isBlank(userDTO.getName()) || Strings.isBlank(userDTO.getPassword()))
            throw new IllegalArgumentException("Validation UserDTO Error");
    }

    public UserInfoDTO transform(User user) {
        UserInfoDTO dto = new UserInfoDTO();
        dto.setId(user.getId());
        dto.setAlarmsAmount(user.getAlarms().size());
        dto.setListsAmount(user.getEventsList().size());
        dto.setName(user.getName());
        dto.setLastAccess(user.getLastAccess());
        return dto;
    }

    public LoginDTO transformToLoginDTO(User user) {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setAdmin(user.getRole().equals(Role.ADMIN));
        loginDTO.setToken(user.getToken());
        return loginDTO;
    }
}
