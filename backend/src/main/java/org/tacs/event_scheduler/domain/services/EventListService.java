package org.tacs.event_scheduler.domain.services;

import org.apache.logging.log4j.util.Strings;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tacs.event_scheduler.domain.entities.Eventbrite;
import org.tacs.event_scheduler.domain.repositories.EventListDao;
import org.tacs.event_scheduler.domain.entities.Event;
import org.tacs.event_scheduler.domain.entities.EventList;
import org.tacs.event_scheduler.domain.entities.user.User;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class EventListService {

    public static final String TODAY = "today";
    public static final String LAST_THREE_DAYS = "lastThreeDays";
    public static final String LAST_WEEK = "lastWeek";
    public static final String LAST_MONTH = "lastMonth";
    public static final String SINCE_BEGINNING_OF_TIME = "sinceBeginningOfTime";


    @Autowired
    private UserService userService;
    @Autowired
    private EventListDao eventListDao;
    @Autowired
    private EventService eventService;

    public void newList(String name, User user) {
        validateName(name, user);
        user.getEventsList().add(new EventList(name));
        this.userService.save(user);
    }

    public void update(Long id, String name, User user) {
        validateName(name, user);
        EventList actualEventList = this.searchEventList(user, id);
        actualEventList.setName(name);
        this.eventListDao.save(actualEventList);
    }

    public void delete(Long id, User user) {
        EventList actualEventList = this.searchEventList(user, id);
        user.getEventsList().remove(actualEventList);
        this.userService.save(user);
    }

    public void addEvent(String eventId, Long id, User user) {
        EventList eventList = this.searchEventList(user, id);
        boolean eventWasNotAdded = eventList.getEventIds().stream().noneMatch(event -> event.equals(eventId));
        
        if (eventWasNotAdded) {
            this.eventService.validate(eventId);
            
            Event event = new Event();
            event.setDateAdded(DateTime.now().toDate());
            event.setEventBriteId(eventId);
            
            eventList.getEvents().add(event);
            this.eventListDao.save(eventList);
        }
    }
    
    //Esto esta nomas para poder testear el history, habria que hacer un wrapper de la date o algo asi para poder testearlo sin tocar el codigo
    public void updateDateAddedFromEvent(String eventId, long eventListId, User user, Date newDate){ 
        EventList eventList = this.searchEventList(user, eventListId);
        
        Optional<Event> eventToUpdate = eventList.getEvents()
        									     .stream()
        									     .filter(event -> event.getEventBriteId().equals(eventId))
        									     .findFirst();
        
        eventToUpdate.get().setDateAdded(newDate);
        eventListDao.save(eventList);
    }
    
    public int getAmountOfEventsByHistoryCriteria(String historyCriteria){
       	DateTime date = DateTime.now();
       	
    	switch (historyCriteria) {
		case TODAY:
			date = date.withTimeAtStartOfDay();
			break;
		case LAST_THREE_DAYS:
			date = date.minusDays(3);
			break;
		case LAST_WEEK:
			date = date.minusDays(7);
			break;
		case LAST_MONTH:
			date = date.minusMonths(1);
			break;
		case SINCE_BEGINNING_OF_TIME:
            return getAmountOfEventsSinceBeginningOfTime();
		default:
			throw new IllegalArgumentException("Invalid Moment :" + historyCriteria);
		}
    	
    	return getAmountOfEventsBeforeDate(date.toDate());
    }    
    
    private int getAmountOfEventsSinceBeginningOfTime(){
    	return (int) eventListDao.findAll()
				   .stream()
				   .map(EventList::getEvents)
                   .flatMap(List::stream)
                   .count();
    }
    
    private int getAmountOfEventsBeforeDate(Date date){
    	return (int) eventListDao.findAll()
						   .stream()
						   .map(EventList::getEvents)
						   .flatMap(List::stream)
						   .filter(event -> date.before(event.getDateAdded()))
						   .count();					   
    }
    
    private void validateName(String name, User user) {
        if (Strings.isBlank(name))
            throw new IllegalArgumentException("Empty event list name");
        boolean duplicateName = user.getEventsList().stream().anyMatch(eventList -> eventList.getName().equals(name));
        if (duplicateName)
            throw new IllegalArgumentException("Duplicate event list name");
    }
    
    private EventList searchEventList(User user, Long id) {
        Optional<EventList> actualEventList = user.getEventsList()
                .stream()
                .filter(eventList -> eventList.getId().equals(id))
                .findFirst();
        if (!actualEventList.isPresent())
            throw new IllegalArgumentException("User: " + user.getId() + " hasn't eventList with id : " + id);
        return actualEventList.get();
    }

    public Map<EventList, List<Eventbrite>> getEvents(User user) {
        return user.getEventsList()
                .stream()
                .collect(Collectors.toMap(Function.identity(), eventList -> this.eventService.getByIds(eventList.getEventIds())));

    }

    public void deleteEvent(Long id, Long eventId, User user) {
        Optional<EventList> eventListOpt = user.getEventsList()
                .stream()
                .filter(eventList -> eventList.getId().equals(id))
                .findFirst();
        if (eventListOpt.isPresent()) {
            EventList eventList = this.eventListDao.findOne(id);
            eventList.setEvents(eventList.getEvents()
            .stream()
            .filter(event -> !event.getId().equals(eventId))
            .collect(Collectors.toList()));
            this.eventListDao.save(eventList);
        } else
            throw new IllegalArgumentException("User :" + user.getName() + " haven't list with id: " + id);

    }
}
