package org.tacs.event_scheduler.domain.services;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tacs.event_scheduler.domain.repositories.UserDao;
import org.tacs.event_scheduler.domain.entities.user.User;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.tacs.event_scheduler.domain.entities.user.Role.ADMIN;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public void save(User user) {
        if (user.getId() == null) {
            User otherUser = this.getByName(user.getName());
            if (otherUser != null)
                throw new IllegalArgumentException("Duplicate Username");
        }
        this.userDao.save(user);
    }

    public List<User> getAll() {
        return this.userDao.findAll();
    }

    public void deleteAll() {
        this.userDao.deleteAll();
    }

    public User get(Long id) {
        User user = this.userDao.findOne(id);
        if (user != null)
            return user;
        else
            throw new IllegalArgumentException("There is no user with id : " + id);
    }

    public User login(String name, String password) {
        User user = this.getByName(name);
        if (user != null && user.getPassword().equals(password)) {
            if (user.getToken() == null)
                user.setToken(UUID.randomUUID().toString());
            user.setLastAccess(DateTime.now().toDate());
            this.userDao.save(user);
            return user;
        } else
            throw new IllegalArgumentException("Username or Password incorrect");
    }

    public void logout(String token) {
        User user = this.getByToken(token);
        if (user != null) {
            user.setToken(null);
            this.userDao.save(user);
        }
    }

    public User getByToken(String token) {
        if (token == null)
            throw new IllegalArgumentException("Empty Token");
        User user = this.userDao.findByToken(token);
        if (user == null)
            throw new IllegalArgumentException("Token not found : " + token );
        return user;
    }

    public void validateAdmin(String token) {
        User user = this.getByToken(token);
        if (!user.getRole().equals(ADMIN))
            throw new IllegalArgumentException("The user : " + user.getName() + " isn't admin");
    }
    
    public int getAmountOfUsersInterestedInEvent(String eventId){
    	return (int) userDao.findAll()
	    				    .stream()
	    				    .filter(user -> user.getEventsIds().contains(eventId))
	    				    .count();
    }

    public List<String> compare(Long id, Long otherId) {
    	if (id.equals(otherId)){
    		throw new IllegalArgumentException("The ids to be compared have to be different");
    	}
    	
        Set<String> userEventsIds = this.get(id).getEventsIds();
        Set<String> otherUserEventsIds  = this.get(otherId).getEventsIds();
        return userEventsIds
                .stream()
                .filter(otherUserEventsIds::contains)
                .collect(Collectors.toList());
    }

    public void validateUser(String token) {
        this.getByToken(token);
    }

    public User getByName(String name) {
        return this.userDao.findByName(name);
    }
}
