package org.tacs.event_scheduler.domain.repositories.category.eventbrite;

import java.util.List;

public class EventbriteCategoryResponse {
    private List<EventbriteCategory> categories;

    public List<EventbriteCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<EventbriteCategory> categories) {
        this.categories = categories;
    }
}
