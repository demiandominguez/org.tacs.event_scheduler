package org.tacs.event_scheduler.domain.repositories;

import org.tacs.event_scheduler.domain.entities.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Long> {
    User findByName(String name);

    User findByToken(String token);
}