package org.tacs.event_scheduler.domain.entities;

import com.google.common.collect.Lists;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import static javax.persistence.CascadeType.ALL;

import java.util.List;
import java.util.stream.Collectors;

@Entity
public class EventList extends AbstractEntity {

	@OneToMany(cascade = ALL)
    private List<Event> events;
    private String name;

    public EventList() {
    }

    public EventList(String name) {
        this.name = name;
    }
    
    public void setEvents(List<Event> events) {
        this.events = events;
    }
    
    public List<Event> getEvents(){
    	if (events == null){
        	events = Lists.newArrayList();
     	 }    	
    	
    	return events;
    }
    
    public List<String> getEventIds() {
        return getEvents().stream().map(Event::getEventBriteId).collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
   
}
