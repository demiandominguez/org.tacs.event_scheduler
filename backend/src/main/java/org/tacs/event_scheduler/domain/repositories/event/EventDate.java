package org.tacs.event_scheduler.domain.repositories.event;

public class EventDate {
    private String local;

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
