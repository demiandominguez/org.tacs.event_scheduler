package org.tacs.event_scheduler.domain.entities.user.alarm;

import org.tacs.event_scheduler.domain.entities.AbstractEntity;

import javax.persistence.Entity;

@Entity
public class Alert extends AbstractEntity {

    private String eventbriteId;
    private String name;
    private String url;

    public String getEventbriteId() {
        return eventbriteId;
    }

    public void setEventbriteId(String eventbriteId) {
        this.eventbriteId = eventbriteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
