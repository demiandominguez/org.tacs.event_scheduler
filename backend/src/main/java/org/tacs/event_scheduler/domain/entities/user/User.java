package org.tacs.event_scheduler.domain.entities.user;

import com.google.common.collect.Lists;
import org.tacs.event_scheduler.domain.entities.AbstractEntity;
import org.tacs.event_scheduler.domain.entities.EventList;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alarm;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static javax.persistence.CascadeType.ALL;

@Entity
public class User extends AbstractEntity {

    private String name;
    private String password;
    private String token;
    @OneToMany(cascade = ALL)
    private List<Alarm> alarms;
    @OneToMany(cascade = ALL)
    private List<EventList> eventsList;
    @Temporal(TemporalType.DATE)
    private Date lastAccess;
    @Enumerated(EnumType.STRING)
    private Role role;

    public User() {
        this.setRole(Role.NORMAL);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Alarm> getAlarms() {
        if (alarms == null)
            alarms = Lists.newArrayList();
        return alarms;
    }

    public void setAlarms(List<Alarm> alarms) {
        this.alarms = alarms;
    }

    public List<EventList> getEventsList() {
        if (eventsList == null)
            eventsList = Lists.newArrayList();
        return eventsList;
    }

    public void setEventsList(List<EventList> eventsList) {
        this.eventsList = eventsList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }
    
    public Set<String> getEventsIds() {
        return this.getEventsList()
                .stream()
                .map(EventList::getEventIds)
                .flatMap(List::stream)
                .distinct()
                .collect(Collectors.toSet());
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
