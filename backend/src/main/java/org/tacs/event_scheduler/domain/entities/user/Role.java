package org.tacs.event_scheduler.domain.entities.user;

public enum Role {
    NORMAL, ADMIN
}
