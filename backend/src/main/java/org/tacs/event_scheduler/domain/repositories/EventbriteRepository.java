package org.tacs.event_scheduler.domain.repositories;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestOperations;

@Component
public class EventbriteRepository {

    @Autowired
    private RestOperations restOperations;
    @Value("${eventbrite.url}")
    private String urlBase;
    @Value("${eventbrite.token}")
    private String token;

    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public <T> T  get(final String path, String parameters, Class<T> responseType){
        String url = urlBase + path + "?token=" + this.token + parameters;
        HttpEntity<String> entity = getHttpEntity();
        try {
            LOGGER.info("GET: " + url);
            ResponseEntity<T> response = this.restOperations.exchange(url, HttpMethod.GET, entity, responseType);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            throw new IllegalArgumentException("Eventbrite response error with url: " + url);
        }
    }

    private HttpEntity<String> getHttpEntity(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer "+token);
        return new HttpEntity<>(headers);
    }

    public void setRestOperations(RestOperations restOperations) {
        this.restOperations = restOperations;
    }
}
