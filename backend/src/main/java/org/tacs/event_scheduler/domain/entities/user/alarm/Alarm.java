package org.tacs.event_scheduler.domain.entities.user.alarm;

import org.tacs.event_scheduler.domain.entities.AbstractEntity;

import com.google.common.collect.Lists;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import static javax.persistence.CascadeType.ALL;

import java.util.Date;
import java.util.List;

@Entity
public class Alarm extends AbstractEntity {

    private String name;
    @OneToOne(cascade = ALL)
    private SearchCriteria criteria;
    @ElementCollection
    private List<Date> notifications;
    @OneToMany(cascade = ALL)
    private List<Alert> lastResult;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SearchCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(SearchCriteria criteria) {
        this.criteria = criteria;
    }
    
	public List<Date> getNotifications() {
		if (notifications == null) {
			notifications = Lists.newArrayList();
		}
		
		return notifications;
	}

	public void setNotifications(List<Date> notifications) {
		this.notifications = notifications;
	}
    
	public List<Alert> getLastResult() {
		if (lastResult == null) {
			lastResult = Lists.newArrayList();
		}
		
		return lastResult;
	}

	public void setLastResult(List<Alert> result) {
		this.lastResult = result;
	}
}
