package org.tacs.event_scheduler.domain.entities.horario;

import javax.persistence.Entity;

import org.joda.time.DateTime;
import org.tacs.event_scheduler.domain.entities.AbstractEntity;

@Entity
public class Horario extends AbstractEntity{

    private DateTime start;
    private DateTime end;
    private String keyword;

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
