package org.tacs.event_scheduler.domain.entities.location;

import org.tacs.event_scheduler.domain.entities.AbstractEntity;

import javax.persistence.Entity;

@Entity
public class Location extends AbstractEntity {

	private String address;
    private Integer distance;

    public Location(String address, Integer distance) {
    	this.address = address;
    	this.distance = distance;
	}

	public Location() {
	}

	public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
    
}
