package org.tacs.event_scheduler.domain.repositories.category.eventbrite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.tacs.event_scheduler.domain.entities.category.Category;
import org.tacs.event_scheduler.domain.entities.category.CategoryEnum;
import org.tacs.event_scheduler.domain.repositories.EventbriteRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EventbriteCategoryRepository {

    @Autowired
    private EventbriteRepository eventbriteRepository;

    @Value("${eventbrite.path.category}")
    private String categoryPath;
    @Value("${eventbrite.path.subcategory}")
    private String subCategoryPath;


    public List<Category> findById(List<String> eventbriteCategoryIds, CategoryEnum categoryEnum) {
        List<EventbriteCategory> categories;
        if (categoryEnum.equals(CategoryEnum.MAIN)) {
            EventbriteCategoryResponse categoryDTO = this.findCategories();
            categories = categoryDTO.getCategories()
                    .stream()
                    .distinct()
                    .filter(category -> eventbriteCategoryIds.contains(category.getId()))
                    .collect(Collectors.toList());
        } else {
            EventbriteSubCategoryResponse subCategoryDTO = this.findSubcategories();
            categories = subCategoryDTO.getSubcategories()
                    .stream()
                    .distinct()
                    .filter(category -> eventbriteCategoryIds.contains(category.getId()))
                    .collect(Collectors.toList());
        }
        return this.transform(categories, categoryEnum);
    }

    private EventbriteSubCategoryResponse findSubcategories() {
        return this.eventbriteRepository.get(this.subCategoryPath, "", EventbriteSubCategoryResponse.class);
    }

    private EventbriteCategoryResponse findCategories() {
        return this.eventbriteRepository.get(this.categoryPath, "",EventbriteCategoryResponse.class);
    }

    private List<Category> transform(List<EventbriteCategory> categories, CategoryEnum categoryEnum) {
        return categories
                .stream()
                .map(eventbriteCategory -> {
                    Category category = new Category();
                    category.setName(eventbriteCategory.getName());
                    category.setEventbriteId(eventbriteCategory.getId());
                    category.setType(categoryEnum);
                    return category;
                })
                .collect(Collectors.toList());
    }

    public List<Category> findAll() {
        List<EventbriteCategory> categories = this.findCategories().getCategories();
        List<EventbriteCategory> subcategories = this.findSubcategories().getSubcategories();
        List<Category> allCategories = this.transform(categories, CategoryEnum.MAIN);
        allCategories.addAll(this.transform(subcategories, CategoryEnum.SUB));
        return allCategories;
    }
}


