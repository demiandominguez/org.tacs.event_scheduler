package org.tacs.event_scheduler.domain.entities;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class Event extends AbstractEntity  {

	private String eventBriteId;
	private Date dateAdded;
	
	public String getEventBriteId() {
		return eventBriteId;
	}

	public void setEventBriteId(String eventBriteId) {
		this.eventBriteId = eventBriteId;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}
	
}
