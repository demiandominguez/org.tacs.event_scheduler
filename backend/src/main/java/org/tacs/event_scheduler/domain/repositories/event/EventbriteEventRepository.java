package org.tacs.event_scheduler.domain.repositories.event;

import org.apache.logging.log4j.util.Strings;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.tacs.event_scheduler.domain.entities.EventbriteBase;
import org.tacs.event_scheduler.domain.entities.user.alarm.SearchCriteria;
import org.tacs.event_scheduler.domain.repositories.EventbriteRepository;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class EventbriteEventRepository {

    @Value("${eventbrite.path.event}")
    private String pathSearch;
    @Value("${eventbrite.path.eventid}")
    private String pathOnlyId;

    @Autowired
    private EventbriteRepository eventbriteRepository;

    public List<EventbriteBase> findByCriteria(SearchCriteria criteria) {
        String parameters = this.constructionQuery(criteria);
        EventbriteEventResponse dto = this.eventbriteRepository.get(this.pathSearch, parameters, EventbriteEventResponse.class);
        return this.transform(dto.getEvents());
    }

    private List<EventbriteBase> transform(List<EventResponse> events) {
        return events
                .stream()
                .distinct()
                .filter(Objects::nonNull)
                .map(event -> {
                    EventbriteBase eventbriteBase = new EventbriteBase();
                    eventbriteBase.setId(event.getId());
                    eventbriteBase.setCategoryId(event.getCategory_id());
                    eventbriteBase.setSubCategoryId(event.getSubcategory_id());
                    eventbriteBase.setUrl(event.getUrl());
                    eventbriteBase.setStartDate(DateTime.parse(event.getStart().getLocal()));
                    eventbriteBase.setEndDate(DateTime.parse(event.getEnd().getLocal()));
                    eventbriteBase.setName(event.getName().getText());
                    eventbriteBase.setFree(event.getIs_free());
                    return eventbriteBase;
                })
                .collect(Collectors.toList());
    }

    public List<EventbriteBase> findByCriteria(List<SearchCriteria> criteriaList) {
        return this.transform(criteriaList
                .stream()
                .map(criteria -> {
                    String parameters = this.constructionQuery(criteria);
                    EventbriteEventResponse dto = this.eventbriteRepository.get(this.pathSearch, parameters, EventbriteEventResponse.class);
                    return dto.getEvents();
                })
                .flatMap(List::stream)
                .collect(Collectors.toList()));
    }

    public List<EventbriteBase> findById(List<String> ids) {
        List<EventResponse> eventResponses = ids
                .stream()
                .distinct()
                .map(this::findById)
                .collect(Collectors.toList());
        return this.transform(eventResponses);
    }

    private EventResponse findById(String id) {
        String path = this.pathOnlyId + id + "/";
        return this.eventbriteRepository.get(path, "", EventResponse.class);
    }

    private String constructionQuery(SearchCriteria searchCriteria) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("YYYY-MM-dd'T'hh:mm:ssZ");
        StringBuilder parameters = new StringBuilder("&");
        if (Strings.isNotBlank(searchCriteria.getKeyword()))
            parameters.append("q=").append(searchCriteria.getKeyword()).append("&");
        if (isNotBlank(searchCriteria.getCategories())) {
            parameters.append("categories=");
            for (String category : searchCriteria.getCategories())
                parameters.append(category).append(",");
            parameters = new StringBuilder(parameters.substring(0, parameters.length() - 1));
            parameters.append("&");
        }
        if (isNotBlank(searchCriteria.getSubcategories())) {
            parameters.append("subcategories=");
            for (String subcategory : searchCriteria.getSubcategories())
                parameters.append(subcategory).append(",");
            parameters = new StringBuilder(parameters.substring(0, parameters.length() - 1));
            parameters.append("&");
        }
        if (Strings.isNotBlank(searchCriteria.getPrice()))
            parameters.append("price=").append(searchCriteria.getPrice()).append("&");
        if (Strings.isNotBlank(searchCriteria.getSortBy()))
            parameters.append("sort_by=").append(searchCriteria.getSortBy()).append("&");
        if (searchCriteria.getStartDate() != null)
            parameters.append("start_date.range_start=").append(new LocalDateTime(searchCriteria.getStartDate()).toString(formatter)).append("&");
        if (searchCriteria.getEndDate() != null)
            parameters.append("start_date.range_end=").append(new LocalDateTime(searchCriteria.getEndDate()).toString(formatter)).append("&");

        parameters.append("page=").append(searchCriteria.getPage());
        return parameters.toString();
    }

    private Boolean isNotBlank(Collection listT) {
        return listT != null && !listT.isEmpty();
    }

    public void validate(String eventId) {
        this.findById(eventId);
    }
}