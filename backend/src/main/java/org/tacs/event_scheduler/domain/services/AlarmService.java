package org.tacs.event_scheduler.domain.services;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tacs.event_scheduler.domain.entities.user.User;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alarm;
import org.tacs.event_scheduler.domain.entities.user.alarm.Alert;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AlarmService {

    @Autowired
    private UserService userService;
    @Autowired
    private EventService eventService;

    public void save(Alarm alarm, User user) {

        if (alarm.getName() == null && alarm.getName().trim().isEmpty()) {
            throw new IllegalArgumentException("Alarm must have a name");
        }
        
        getLastResult(alarm);
        user.getAlarms().add(alarm);
        this.userService.save(user);
    }

    // 86400000ms = 24hrs TODO: Configurar desde el application.properties o desde alguna variable para poder testearlo
    @Scheduled(fixedDelay = 86400000)
    public void getAlerts() {
        userService.getAll()
                   .forEach(this::getAlerts);
    }

    private void getAlerts(User user) {
        user.getAlarms()
            .forEach(alarm -> {
            	getLastResult(alarm);
            });

        userService.save(user);
    }
    
    private void getLastResult(Alarm alarm){
    	List<Alert> newResult = eventService.getEventbriteBase(alarm.getCriteria())
                 .stream()
                 .map(eventbriteBase -> {
                     Alert alert = new Alert();
                     alert.setName(eventbriteBase.getName());
                     alert.setEventbriteId(eventbriteBase.getId());
                     alert.setUrl(eventbriteBase.getUrl());
                     return alert;
                 }).collect(Collectors.toList());
    	
    	 List<String> newResultIds = newResult.stream()
    			 							  .map(Alert::getEventbriteId)
    			 							  .collect(Collectors.toList());

         List<String> oldResultsIds = alarm.getLastResult()
										   .stream()
										   .map(Alert::getEventbriteId)
										   .collect(Collectors.toList());
											
											
         boolean hasNewEvents = !oldResultsIds.containsAll(newResultIds);

         if (hasNewEvents) {
             Date now = DateTime.now().toDate();
             alarm.getNotifications().add(now);
         }

         alarm.setLastResult(newResult);
    }
}
