package org.tacs.event_scheduler.domain.entities.category;

import org.tacs.event_scheduler.domain.entities.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Category extends AbstractEntity {

    private String name;
    private String eventbriteId;
    @Enumerated(EnumType.STRING)
    private CategoryEnum type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventbriteId() {
        return eventbriteId;
    }

    public void setEventbriteId(String eventbriteId) {
        this.eventbriteId = eventbriteId;
    }

    public CategoryEnum getType() {
        return type;
    }

    public void setType(CategoryEnum type) {
        this.type = type;
    }
}
