package org.tacs.event_scheduler.domain.repositories.event;

import java.util.List;

public class EventbriteEventResponse {
        private List<EventResponse> events;

        public List<EventResponse> getEvents() {
            return events;
        }

        public void setEvents(List<EventResponse> events) {
            this.events = events;
        }
}
