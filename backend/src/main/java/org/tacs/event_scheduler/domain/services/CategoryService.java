package org.tacs.event_scheduler.domain.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tacs.event_scheduler.domain.entities.category.Category;
import org.tacs.event_scheduler.domain.entities.category.CategoryEnum;
import org.tacs.event_scheduler.domain.repositories.category.CategoryDao;
import org.tacs.event_scheduler.domain.repositories.category.eventbrite.EventbriteCategoryRepository;
import org.tacs.event_scheduler.domain.entities.EventbriteBase;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.tacs.event_scheduler.domain.entities.category.CategoryEnum.MAIN;
import static org.tacs.event_scheduler.domain.entities.category.CategoryEnum.SUB;

@Service
public class CategoryService {

    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private EventbriteCategoryRepository eventbriteCategoryRepository;

    public List<Category> findCategories(List<EventbriteBase> eventbriteBaseList) {
        List<String> eventbriteCategoryIds = eventbriteBaseList
                .stream()
                .map(EventbriteBase::getCategoryId)
                .distinct()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        List<Category> categories = this.categoryDao.findByTypeAndEventbriteIdIn(MAIN, eventbriteCategoryIds);
        if (categories.size() < eventbriteCategoryIds.size()) {
            List<String> eventbriteIds = categories
                    .stream()
                    .map(Category::getEventbriteId)
                    .collect(Collectors.toList());
            List<String> newCategoryIds = eventbriteCategoryIds
                    .stream()
                    .filter(eventbriteCategoryId -> !eventbriteIds.contains(eventbriteCategoryId))
                    .collect(Collectors.toList());
            categories.addAll(this.findCategory(newCategoryIds, MAIN));
        }
        return categories;
    }

    private List<Category> findCategory(List<String> eventbriteCategoryIds, CategoryEnum categoryEnum) {
        List<Category> categories = this.eventbriteCategoryRepository.findById(eventbriteCategoryIds, categoryEnum);
        return this.categoryDao.save(categories);
    }

    public List<Category> findSubcategories(List<EventbriteBase> eventbriteBaseList) {
        List<String> eventbriteSubCategoryIds = eventbriteBaseList
                .stream()
                .map(EventbriteBase::getSubCategoryId)
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());
        List<Category> subCategories = this.categoryDao.findByTypeAndEventbriteIdIn(SUB, eventbriteSubCategoryIds);
        if (subCategories.size() < eventbriteSubCategoryIds.size()) {
            List<String> eventbriteIds = subCategories
                    .stream()
                    .map(Category::getEventbriteId)
                    .collect(Collectors.toList());
            List<String> newCategoryIds = eventbriteSubCategoryIds
                    .stream()
                    .filter(eventbriteSubCategoryId -> !eventbriteIds.contains(eventbriteSubCategoryId))
                    .collect(Collectors.toList());
            subCategories.addAll(this.findCategory(newCategoryIds, SUB));
        }
        return subCategories;
    }

    public List<Category> getCategoriesAndSubCategories() {
        List<Category> categories = this.categoryDao.findAll();
        if (categories.isEmpty()) {
            categories = this.getAll();
            return this.categoryDao.save(categories);
        } else
            return categories;
    }

    private List<Category> getAll() {
        return this.eventbriteCategoryRepository.findAll();
    }
}
