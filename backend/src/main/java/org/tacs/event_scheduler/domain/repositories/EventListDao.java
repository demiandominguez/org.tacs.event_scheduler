package org.tacs.event_scheduler.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tacs.event_scheduler.domain.entities.EventList;

public interface EventListDao extends JpaRepository<EventList, Long> {
}