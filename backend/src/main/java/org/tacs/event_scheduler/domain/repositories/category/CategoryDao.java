package org.tacs.event_scheduler.domain.repositories.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tacs.event_scheduler.domain.entities.category.Category;
import org.tacs.event_scheduler.domain.entities.category.CategoryEnum;

import java.util.List;

public interface CategoryDao extends JpaRepository<Category, Long> {

    List<Category> findByTypeAndEventbriteIdIn(CategoryEnum type, List<String> eventbriteSubCategoryIds);
}
