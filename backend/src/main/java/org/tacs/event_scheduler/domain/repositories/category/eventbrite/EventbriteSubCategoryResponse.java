package org.tacs.event_scheduler.domain.repositories.category.eventbrite;

import java.util.List;

public class EventbriteSubCategoryResponse {
    private List<EventbriteCategory> subcategories;

    public List<EventbriteCategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<EventbriteCategory> subcategories) {
        this.subcategories = subcategories;
    }

}
