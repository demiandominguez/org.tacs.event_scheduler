package org.tacs.event_scheduler.domain.services;

import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tacs.event_scheduler.domain.entities.Eventbrite;
import org.tacs.event_scheduler.domain.entities.EventbriteBase;
import org.tacs.event_scheduler.domain.entities.category.Category;
import org.tacs.event_scheduler.domain.entities.user.alarm.SearchCriteria;
import org.tacs.event_scheduler.domain.repositories.event.EventbriteEventRepository;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class EventService {

    @Autowired
    private EventbriteEventRepository eventbriteEventRepository;
    @Autowired
    private CategoryService categoryService;

    public List<Eventbrite> get(List<SearchCriteria> criteriaList) {
        List<EventbriteBase> eventbriteBaseList = this.eventbriteEventRepository.findByCriteria(criteriaList);
        return this.merge(eventbriteBaseList);
    }

    public List<String> getEventIds(SearchCriteria criteria) {
        return this.eventbriteEventRepository.findByCriteria(criteria)
                .stream()
                .map(EventbriteBase::getId)
                .distinct()
                .collect(Collectors.toList());
    }

    public List<EventbriteBase> getEventbriteBase(SearchCriteria criteria) {
        return this.eventbriteEventRepository.findByCriteria(criteria);
    }

    public List<Eventbrite> get(SearchCriteria criteria) {
        List<EventbriteBase> eventbriteBaseList = this.getEventbriteBase(criteria);
        return this.merge(eventbriteBaseList);
    }

    private List<Eventbrite> merge(List<EventbriteBase> eventbriteBaseList) {
        List<Category> categories = this.categoryService.findCategories(eventbriteBaseList);
        List<Category> subCategories = this.categoryService.findSubcategories(eventbriteBaseList);
        Map<String, Category> categoriesMap = categories
                .stream()
                .collect(Collectors.toMap(Category::getEventbriteId, Function.identity()));
        Map<String, Category> subCategoriesMap = subCategories
                .stream()
                .collect(Collectors.toMap(Category::getEventbriteId, Function.identity()));
        return eventbriteBaseList
                .stream()
                .map(eventbriteBase -> {
                    Eventbrite eventbrite = new Eventbrite();
                    if (eventbriteBase.getCategoryId() != null)
                        eventbrite.setCategory(categoriesMap.get(eventbriteBase.getCategoryId()));
                    if (eventbriteBase.getSubCategoryId() != null)
                        eventbrite.setSubCategory(subCategoriesMap.get(eventbriteBase.getSubCategoryId()));
                    eventbrite.setId(eventbriteBase.getId());
                    eventbrite.setName(eventbriteBase.getName());
                    eventbrite.setUrl(eventbriteBase.getUrl());
                    eventbrite.setStartDate(eventbriteBase.getStartDate());
                    eventbrite.setEndDate(eventbriteBase.getEndDate());
                    eventbrite.setFree(eventbriteBase.getFree());
                    return eventbrite;
                }).collect(Collectors.toList());
    }

    public void validate(String eventId) {
        this.eventbriteEventRepository.validate(eventId);
    }

    public List<Eventbrite> getByIds(List<String> ids) {
        return this.merge(this.eventbriteEventRepository.findById(ids));
    }
}
