package Bots;

import Services.EventService;
import Services.ListService;
import Services.UserService;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.objects.Ability;


import static org.telegram.abilitybots.api.objects.Locality.ALL;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;

public class EventSchedulerBot extends AbilityBot {

    private ListService listService;
    private EventService eventService;
    private UserService userService;

    public EventSchedulerBot() {
        super(BOT_TOKEN, BOT_USERNAME);
        this.listService = new ListService();
        this.eventService = new EventService();
        this.userService = new UserService();
    }

    public static String BOT_TOKEN = "657166985:AAH_5V5FoQvpdzab41YTWD6U0MafHWW2h-0";
    public static String BOT_USERNAME = "Tacs4EventSchedulerBot";

    public Ability GetLists() {
        return Ability
                .builder()
                .name("mylists")
                .info("Get user event lists")
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> silent.send(this.listService.getLists(ctx.user().getUserName()).toString(), ctx.chatId()))
                .build();
    }

    public Ability GetEventsInList() {
        return Ability
                .builder()
                .name("listevents")
                .info("Given a list name return the events in it")
                .input(0)
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> silent.send(this.listService.getListEvents(ctx.user().getUserName(), ctx.arguments()).toString(), ctx.chatId()))
                .build();
    }

    public Ability SearchEventsByKeyword() {
        return Ability
                .builder()
                .name("search")
                .info("Given a keyword return the next events that are releated to the keyword")
                .input(1)
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> silent.send(this.eventService.SearchEvents(ctx.user().getUserName(), ctx.firstArg()), ctx.chatId()))
                .build();
    }

    public Ability AddEventToList() {
        return Ability
                .builder()
                .name("add")
                .info("Given a list name and an event id, adds the event to the list")
                .input(0)
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> silent.send(this.listService.addEventToList(ctx.user().getUserName(), ctx.arguments()), ctx.chatId()))
                .build();
    }

    public Ability Login() {
        return Ability
                .builder()
                .name("login")
                .info("Login using username and password")
                .input(2)
                .locality(ALL)
                .privacy(PUBLIC)
                .action(ctx -> silent.send(this.userService.login(ctx.user().getUserName(), ctx.firstArg(), ctx.secondArg()), ctx.chatId()))
                .build();
    }


    public int creatorId() {
        return 243811881;
    }

}
