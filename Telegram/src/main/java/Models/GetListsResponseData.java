package Models;

import java.util.List;

public class GetListsResponseData {

    public int id;
    public String name;
    public List<Event> events;

}
