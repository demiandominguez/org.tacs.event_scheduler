package Models;

import java.util.List;

public class SearchEventsResponseContent {

    public String message;
    public List<Event> data;
}
