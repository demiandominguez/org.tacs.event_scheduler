package Models;

public class HttpResponse {

    public int statusCode;
    public String content;

    public HttpResponse(int statusCode, String content) {
        this.statusCode = statusCode;
        this.content = content;
    }
}
