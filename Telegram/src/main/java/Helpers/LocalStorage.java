package Helpers;

import java.util.HashMap;

public class LocalStorage {

    private static HashMap<String, String> storage = new HashMap<>();

    public static void Set(String key, String value) {
        storage.put(key, value);
    }

    public static String get(String key) {
        return storage.get(key);
    }


}
