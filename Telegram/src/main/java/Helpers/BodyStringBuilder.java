package Helpers;

import java.util.HashMap;

public class BodyStringBuilder {

    public static String getBodyStrin(HashMap<String, String> pairs) {

        StringBuilder bodyStringBuilder = new StringBuilder();

        bodyStringBuilder.append("{");

        pairs.forEach( (key, value) -> {
            bodyStringBuilder.append("\"")
                    .append(key)
                    .append("\"")
                    .append(":")
                    .append("\"")
                    .append(value)
                    .append("\"")
                    .append(",");
        });

        bodyStringBuilder.deleteCharAt(bodyStringBuilder.lastIndexOf(","));

        bodyStringBuilder.append("}");

        return bodyStringBuilder.toString();
    }
}
