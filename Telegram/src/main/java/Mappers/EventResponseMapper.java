package Mappers;

import Models.Event;
import Models.GetListsResponseContent;
import Models.GetListsResponseData;
import Models.SearchEventsResponseContent;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class EventResponseMapper {

    public static String mapEventsFromResponse(String responseContent) {
        List<Event> listEvents = new ArrayList<>();
        SearchEventsResponseContent responseContentObject;

        Gson g = new Gson();

        responseContentObject = g.fromJson(responseContent, SearchEventsResponseContent.class);

        String eventsResponse;
        StringBuilder eventsStringBuilder = new StringBuilder();

        for (Event event : responseContentObject.data) {
            eventsStringBuilder.append("Name: ")
                    .append(event.name)
                    .append("\n")
                    .append("ID: ")
                    .append(event.id)
                    .append("\n")
                    .append("URL: ")
                    .append(event.url)
                    .append("\n\n");
        }

        if(!responseContentObject.data.isEmpty()) {
            eventsStringBuilder.append("To discover more events please visit our website.");
        } else {
            eventsStringBuilder.append("No events found for keyword. Please try with another");
        }

        eventsResponse = eventsStringBuilder.toString();

        return  eventsResponse;
    }
}
