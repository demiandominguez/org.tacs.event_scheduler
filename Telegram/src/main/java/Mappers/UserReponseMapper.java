package Mappers;

import Models.LoginResponseContent;
import Models.SearchEventsResponseContent;
import com.google.gson.Gson;

public class UserReponseMapper {

    public static String GetToken(String responseContent) {
        LoginResponseContent responseContentObject;

        Gson g = new Gson();

        responseContentObject = g.fromJson(responseContent, LoginResponseContent.class);

        return responseContentObject.data.token;

    }
}
