package Mappers;

import Models.Event;
import Models.GetListsResponseContent;
import Models.GetListsResponseData;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class ListResposeMapper {

    public static String mapFromListsResponse(String responseContent) {

        StringBuilder listStringBuilder = new StringBuilder();
        GetListsResponseContent responseContentObject;

        Gson g = new Gson();

        responseContentObject = g.fromJson(responseContent, GetListsResponseContent.class);

        for (GetListsResponseData listData : responseContentObject.data) {
            listStringBuilder.append("-")
                    .append(listData.name)
                    .append("\n");
        }


        return  listStringBuilder.toString();
    }

    public static List<Event> mapEventsFromListsResponse(String responseContent, String listName) {

        List<Event> listEvents = new ArrayList<>();
        GetListsResponseContent responseContentObject;

        Gson g = new Gson();

        responseContentObject = g.fromJson(responseContent, GetListsResponseContent.class);

        for (GetListsResponseData listData : responseContentObject.data) {
            String dataListName = listData.name.replaceAll(" ", "");
            if (dataListName.equalsIgnoreCase(listName)) {
                listEvents = listData.events;
                break;
            }
        }

        return  listEvents;
    }

    public static int GetListIdFromResponse(String responseContent, String listName) {
        GetListsResponseContent responseContentObject;
        int listId = -1;

        Gson g = new Gson();

        responseContentObject = g.fromJson(responseContent, GetListsResponseContent.class);

        for (GetListsResponseData listData : responseContentObject.data) {
            if (listData.name.equalsIgnoreCase(listName)) {
                listId = listData.id;
                break;
            }
        }

        return  listId;
    }
}
