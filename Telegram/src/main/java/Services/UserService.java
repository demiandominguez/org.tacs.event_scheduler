package Services;

import Helpers.LocalStorage;
import Mappers.UserReponseMapper;
import Models.HttpResponse;

import java.util.HashMap;

public class UserService {

    private HttpService http;
    private String apiBaseUrl = "users";

    public UserService() {
        this.http = new HttpService();
    }

    public String login(String user, String userName, String password) {

        HashMap<String, String> bodyPairs = new HashMap<>();

        bodyPairs.put("name", userName);
        bodyPairs.put("password", password);

        HttpResponse postResponse = http.post(this.apiBaseUrl + "/login", bodyPairs, null);

        if(postResponse.statusCode == 200) {
            String token = UserReponseMapper.GetToken(postResponse.content);
            LocalStorage.Set(user, token);
            return "Login successful";
        } else {
            return "Something went wrong. Please try again later.";
        }

    }
}
