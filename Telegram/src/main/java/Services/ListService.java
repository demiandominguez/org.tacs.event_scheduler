package Services;

import Helpers.LocalStorage;
import Mappers.ListResposeMapper;
import Models.Event;
import Models.HttpResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ListService {

    private HttpService http;
    private String apiBaseUrl = "users/event-lists";

    public ListService() {
        this.http = new HttpService();
    }

    public String getLists(String user) {

        HashMap<String, String> params = new HashMap<>();

        String token = LocalStorage.get(user);

        if(token == null) {
            return "You must login before making requests";
        }

        HttpResponse response = this.http.get(this.apiBaseUrl, params, token);
        String listsResponse;

        if(response.statusCode == 200) {
            listsResponse = ListResposeMapper.mapFromListsResponse(response.content);
        } else {
            listsResponse = "Something went wrong. Please try again later";
        }

        return listsResponse;

    }

    public String getListEvents(String user, String[] args) {

        List<Event> events = new ArrayList<>();
        HashMap<String, String> params = new HashMap<>();

        String listName;
        StringBuilder argsStringBuilder = new StringBuilder();

        Arrays.stream(args).forEach( arg -> argsStringBuilder.append(arg));

        listName = argsStringBuilder.toString();

        String token = LocalStorage.get(user);

        if(token == null) {
            return "You must login before making requests";
        }

        HttpResponse response = this.http.get(this.apiBaseUrl, params, token);

        if(response.statusCode == 200) {
            events = ListResposeMapper.mapEventsFromListsResponse(response.content, listName);
        } else {
            events = new ArrayList<>();
        }

        String eventsResponse;
        StringBuilder responseStringBuilder = new StringBuilder();

        events.forEach(event -> {
            responseStringBuilder.append("Nombre: ")
                    .append(event.name)
                    .append("\n")
                    .append("ID: ")
                    .append(event.id)
                    .append("\n")
                    .append("URL: ")
                    .append(event.url)
                    .append("\n\n");
        });

        eventsResponse = responseStringBuilder.toString();

        return eventsResponse;
    }

    public String addEventToList(String user, String[] args) {

        long eventId = Long.parseLong(args[args.length - 1]);
        String listName = this.getNameFromArgs(args);

        HashMap<String, String> bodyPairs = new HashMap<>();
        HashMap<String, String> params = new HashMap<>();

        String token = LocalStorage.get(user);

        if(token == null) {
            return "You must login before making requests";
        }

        HttpResponse response = this.http.get(this.apiBaseUrl, params, token);

        int listId;

        if(response.statusCode == 200) {
           listId = ListResposeMapper.GetListIdFromResponse(response.content, listName);
        } else {
            return "Something went wrong. Please try again later.";
        }

        if(listId == -1){
            return "No list found with name: "+ listName;
        }

        bodyPairs.put("eventId", String.valueOf(eventId));
        bodyPairs.put("eventListId", String.valueOf(listId));


        HttpResponse postResponse = http.post(this.apiBaseUrl + "/event", bodyPairs, token);

        if(postResponse.statusCode == 200) {
            return "Event: " + eventId + " successfully added to list " + listName;
        } else {
            return "Something went wrong. Please try again later.";
        }
    }

    private String getNameFromArgs(String[] args) {
        StringBuilder listNameBuilder = new StringBuilder();
        int counter = 0;
        int lastListWord = args.length - 2;

        for(int i = 0; i <= lastListWord; i++) {
            listNameBuilder.append(args[i]);

            if (i != lastListWord) {
                listNameBuilder.append(" ");
            }
        }

        return  listNameBuilder.toString();
    }


}
