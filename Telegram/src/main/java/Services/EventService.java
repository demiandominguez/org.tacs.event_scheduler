package Services;

import Helpers.LocalStorage;
import Mappers.EventResponseMapper;
import Models.HttpResponse;

import java.util.HashMap;

public class EventService {

    private HttpService http;
    private String apiBaseUrl = "events";

    public EventService() {
        this.http = new HttpService();
    }

    public String SearchEvents(String user, String keyword) {

        HashMap<String, String> params = new HashMap<>();
        params.put("keyword", keyword);
        params.put("maxQuantity", "5");
        params.put("sortBy", "date");

        String token = LocalStorage.get(user);

        if(token == null) {
            return "You must login before making requests";
        }

        HttpResponse response = this.http.get(this.apiBaseUrl, params, token);

        String eventsResponse;

        if(response.statusCode == 200) {
            eventsResponse = EventResponseMapper.mapEventsFromResponse(response.content);
        } else {
            eventsResponse = "Something went wrong, please try again later";
        }



        return eventsResponse;
    }

}
