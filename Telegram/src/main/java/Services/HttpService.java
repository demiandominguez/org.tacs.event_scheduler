package Services;

import Helpers.BodyStringBuilder;
import Helpers.ParameterStringBuilder;
import Models.HttpResponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class HttpService {

    private String baseUrl = "http://35.194.61.236/";

    public HttpResponse get(String api, HashMap<String, String> params, String token) {

        HttpResponse response;

        try {

           //Create parameters
           String queryParams = ParameterStringBuilder.getParamsString(params);


            //Create URL
            URL url = new URL(this.baseUrl + api + queryParams);

            //Create connection
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");

            //Set Headers
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("token", token);


            response = this.doCall(connection);

            connection.disconnect();
        } catch (Exception e) {
            response = this.createInternalServerErrorResponse();
        }

        return  response;
    }

    private HttpResponse doCall(HttpURLConnection connection) {

        HttpResponse response;

        try {
            int statusCode = connection.getResponseCode();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;

            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();

            response = new HttpResponse(statusCode, content.toString());
        } catch (Exception e) {
            response = this.createInternalServerErrorResponse();
        }

        return  response;
    }

    public HttpResponse post(String api, HashMap<String, String> bodyPairs, String token){

        HttpResponse response;

        try {
            //Create URL
            URL url = new URL(this.baseUrl + api);

            //Create connection
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");

            connection.setDoOutput (true);

            //Set Headers
            connection.setRequestProperty("Content-Type", "application/json");
            if(token != null) {
                connection.setRequestProperty("token", token);
            }

            DataOutputStream output = new DataOutputStream(connection.getOutputStream());

            /*Construct the POST data.*/
            String content = BodyStringBuilder.getBodyStrin(bodyPairs);

            /* Send the request data.*/
            output.writeBytes(content);
            output.flush();
            output.close();


            response = this.doCall(connection);

            connection.disconnect();
        } catch (Exception e) {
            response = this.createInternalServerErrorResponse();
        }

        return  response;

    }

    private HttpResponse createInternalServerErrorResponse(){
        return new HttpResponse(500, "Internal server error");
    }


}
